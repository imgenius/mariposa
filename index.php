<?php

// change the following paths if necessary
//$yii=dirname(__FILE__).'/../yiibuzz/framework/yii.php';
$yii=dirname(__FILE__).'/../yii/framework/yii.php';
$hostname = $_SERVER['SERVER_NAME'];

// remove the following lines when in production mode
defined('YII_DEBUG') or define('YII_DEBUG',true);
// specify how many levels of call stack should be shown in each log message
defined('YII_TRACE_LEVEL') or define('YII_TRACE_LEVEL',3);

require_once($yii);
require_once(dirname(__FILE__) . '/protected/helpers/shortcuts.php');
//~ echo $hostname;
if ((strpos($hostname, '127.0.0.1') === false) && (strpos($hostname, 'local') === false)) {

$config=dirname(__FILE__).'/protected/config/main.php';
} else {
		$config=dirname(__FILE__).'/protected/config/local.php'; //allow different db connection
}
Yii::createWebApplication($config)->run();
