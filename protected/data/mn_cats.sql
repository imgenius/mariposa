-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jun 03, 2013 at 10:16 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `testname_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_article_category`
--


INSERT INTO `tbl_article_category` (`cat_id`, `name`, `slug`, `create_time`, `create_user_id`, `update_time`, `update_user_id`, `state_id`, `parent_id`, `priority`, `publish`, `revision`) VALUES
(23, 'MN District Court Judges (by District)', NULL, NULL, NULL, NULL, NULL, 1, 0, 110, 1, 0),
(24, 'Tribal Courts', NULL, NULL, NULL, NULL, NULL, 1, 0, 160, 1, 0),
(26, 'Professional Associations', NULL, NULL, NULL, NULL, NULL, 1, 0, 220, 1, 0),
(27, 'Federal Courts', NULL, NULL, NULL, NULL, NULL, 1, 0, 120, 1, 0),
(28, 'Municipal Information', NULL, NULL, NULL, NULL, NULL, 1, 0, 210, 1, 0),
(29, 'MN Court Reporters', NULL, NULL, NULL, NULL, NULL, 1, 0, 230, 1, 0),
(30, 'MN Libraries', NULL, NULL, NULL, NULL, NULL, 1, 0, 240, 1, 0),
(31, 'MN Hospitals', NULL, NULL, NULL, NULL, NULL, 1, 0, 250, 1, 0),
(32, 'MN Medical Clinics', NULL, NULL, NULL, NULL, NULL, 1, 0, 260, 1, 0),
(33, 'Minnesota Newspapers', NULL, NULL, NULL, NULL, NULL, 1, 0, 180, 1, 0),
(34, 'Insurance Companies', NULL, NULL, NULL, NULL, NULL, 1, 0, 270, 1, 0),
(35, 'Correctional Facilities', NULL, NULL, NULL, NULL, NULL, 1, 0, 290, 1, 0),
(36, 'Federal and State Legislators', NULL, NULL, NULL, NULL, NULL, 1, 0, 300, 1, 0),
(37, 'Courts and Government Offices (Alabama–Wyoming)', NULL, NULL, NULL, NULL, NULL, 1, 0, 310, 1, 0),
(39, 'Mileage Tables', NULL, NULL, NULL, NULL, NULL, 1, 0, 340, 1, 0),
(40, 'Area Codes', NULL, NULL, NULL, NULL, NULL, 1, 0, 360, 1, 0),
(43, 'Federal Government Offices', NULL, NULL, NULL, NULL, NULL, 1, 0, 130, 1, 0),
(45, 'MN Appellate Courts', NULL, NULL, NULL, NULL, NULL, 1, 0, 140, 1, 0),
(46, 'State of MN', NULL, NULL, NULL, NULL, NULL, 1, 0, 150, 1, 0),
(47, 'Fees', NULL, NULL, NULL, NULL, NULL, 1, 0, 190, 1, 0),
(48, 'City/County/ZIP Code Directory', NULL, NULL, NULL, NULL, NULL, 1, 0, 330, 1, 0),
(50, 'Calendars', NULL, NULL, NULL, NULL, NULL, 1, 0, 370, 1, 0),
(51, 'Maps', NULL, NULL, NULL, NULL, NULL, 1, 0, 350, 1, 0),
(52, 'Court Filing Rules', NULL, NULL, NULL, NULL, NULL, 1, 0, 200, 1, 0),
(57, 'Legal Services', NULL, NULL, NULL, NULL, NULL, 1, 0, 390, 1, 0),
(58, 'County Information', NULL, NULL, NULL, NULL, NULL, 1, 0, 170, 1, 0),
(59, 'Deadline and Expiration Date Calculator', NULL, NULL, NULL, NULL, NULL, 1, 0, 380, 1, 0),
(60, 'MN Police Departments', NULL, NULL, NULL, NULL, NULL, 1, 0, 280, 1, 0),
(62, 'MN Abstracters', NULL, NULL, NULL, NULL, NULL, 1, 0, 175, 1, 0),
(65, 'MN Judges (Alphabetical)', NULL, NULL, NULL, NULL, NULL, 1, 0, 100, 1, 0),
(66, 'Aitkin County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(67, 'Le Sueur County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(68, 'Yellow Medicine County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(69, 'Lake of the Woods County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(70, 'Lake County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(71, 'Lac qui Parle County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(72, 'Koochiching County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(73, 'Kittson County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(74, 'Kandiyohi County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(75, 'Kanabec County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(76, 'Jackson County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(77, 'Itasca County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(78, 'Isanti County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(79, 'Hubbard County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(80, 'Houston County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(81, 'Grant County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(82, 'Goodhue County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(83, 'Freeborn County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(84, 'Fillmore County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(85, 'Faribault County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(86, 'Douglas County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(87, 'Dodge County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(88, 'Crow Wing County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(89, 'Cottonwood County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(90, 'Cook County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(91, 'Clearwater County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(92, 'Clay County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(93, 'Chisago County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(94, 'Chippewa County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(95, 'Cass County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(96, 'Carver County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(97, 'Carlton County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(98, 'Brown County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(99, 'Blue Earth County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(100, 'Big Stone County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(101, 'Benton County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(102, 'Beltrami County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(103, 'Becker County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(104, 'Winona County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(105, 'Wilkin County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(106, 'Watonwan County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(107, 'Waseca County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(108, 'Wadena County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(109, 'Wabasha County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(110, 'Traverse County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(111, 'Todd County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(112, 'Swift County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(113, 'Stevens County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(114, 'Steele County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(115, 'Stearns County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(116, 'Sibley County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(117, 'Sherburne County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(118, 'Scott County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(119, 'St. Louis County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(120, 'Roseau County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(121, 'Rock County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(122, 'Rice County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(123, 'Renville County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(124, 'Redwood County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(125, 'Red Lake County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(126, 'Pope County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(127, 'Polk County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(128, 'Pipestone County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(129, 'Pine County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(130, 'Pennington County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(131, 'Otter Tail County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(132, 'Olmsted County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(133, 'Norman County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(134, 'Nobles County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(135, 'Nicollet County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(136, 'Murray County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(137, 'Mower County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(138, 'Morrison County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(139, 'Mille Lacs County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(140, 'Meeker County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(141, 'McLeod County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(142, 'Martin County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(143, 'Marshall County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(144, 'Mahnomen County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(145, 'Lyon County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(146, 'Lincoln County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(147, 'Law and Reference Libraries', NULL, NULL, NULL, NULL, NULL, 1, 30, 100, 1, 0),
(148, 'Medical Libraries', NULL, NULL, NULL, NULL, NULL, 1, 30, 110, 1, 0),
(149, 'Ninth Judicial District', NULL, NULL, NULL, NULL, NULL, 1, 23, 190, 1, 0),
(150, 'Public Libraries', NULL, NULL, NULL, NULL, NULL, 1, 30, 120, 1, 0),
(151, 'Mileage Table I', NULL, NULL, NULL, NULL, NULL, 1, 39, 10, 1, 0),
(152, 'By Location', NULL, NULL, NULL, NULL, NULL, 1, 40, 100, 1, 0),
(153, 'Federal Court Fees', NULL, NULL, NULL, NULL, NULL, 1, 47, 100, 1, 1),
(154, 'Eighth Judicial District', NULL, NULL, NULL, NULL, NULL, 1, 23, 180, 1, 0),
(155, 'Seventh Judicial District', NULL, NULL, NULL, NULL, NULL, 1, 23, 170, 1, 0),
(156, 'Sixth Judicial District', NULL, NULL, NULL, NULL, NULL, 1, 23, 160, 1, 0),
(157, 'Fifth Judicial District', NULL, NULL, NULL, NULL, NULL, 1, 23, 150, 1, 0),
(158, 'Fourth Judicial District', NULL, NULL, NULL, NULL, NULL, 1, 23, 140, 1, 0),
(159, 'Third Judicial District', NULL, NULL, NULL, NULL, NULL, 1, 23, 120, 1, 0),
(160, 'Second Judicial District', NULL, NULL, NULL, NULL, NULL, 1, 23, 110, 1, 0),
(161, 'First Judicial District', NULL, NULL, NULL, NULL, NULL, 1, 23, 100, 1, 0),
(162, 'Education, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(163, 'Agriculture, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(164, 'Commerce, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(165, 'Energy, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(166, 'General Services Administration', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(167, 'Government Printing Office', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(168, 'Treasury, Department of the', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(169, 'Wright County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(170, 'Mileage Table II', NULL, NULL, NULL, NULL, NULL, 1, 39, 20, 1, 0),
(171, 'Numeric Area Codes', NULL, NULL, NULL, NULL, NULL, 1, 40, 110, 1, 0),
(172, 'Commodity Futures Trading Commission', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(173, 'County Fees', NULL, NULL, NULL, NULL, NULL, 1, 47, 130, 1, 1),
(174, 'County Law Libraries', NULL, NULL, NULL, NULL, NULL, 1, 30, 105, 1, 0),
(175, 'Defense, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(176, 'Commission on Civil Rights', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(177, 'Consumer Product Safety Commission', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(178, 'Environmental Protection Agency', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(179, 'Equal Employment Opportunity Commission', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(180, 'Farm Credit Administration', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(181, 'Federal Deposit Insurance Corporation', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(182, 'Federal Communications Commission', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(183, 'Federal Mediation and Conciliaton Service', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(184, 'National Transportation Safety Board', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(185, 'Federal Trade Commission', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(186, 'Health and Human Services, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(187, 'Housing and Urban Development, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(188, 'Interior, Department of the', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(189, 'International Trade Commission', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(190, 'Justice, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(191, 'Labor, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(192, 'National Archives and Records Administration', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(193, 'National Labor Relations Board', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(194, 'State, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(195, 'Securities and Exchange Commission', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(196, 'Small Business Administration', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(197, 'Social Security Administration', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(198, 'Transportation, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(199, 'Hennepin County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(200, 'Veterans Affairs, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(201, 'Ramsey County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(202, 'Washington County', NULL, NULL, NULL, NULL, NULL, 1, 58, 200, 1, 0),
(203, 'State Court Standard Fees', NULL, NULL, NULL, NULL, NULL, 1, 47, 110, 1, 1),
(204, 'Secretary of State Fees', NULL, NULL, NULL, NULL, NULL, 1, 47, 120, 1, 1),
(205, 'Central Intelligence Agency', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(206, 'Recorder and Registrar of Title Standard Fees', NULL, NULL, NULL, NULL, NULL, 1, 47, 115, 1, 0),
(207, 'Minnesota Court of Appeals Judges', NULL, NULL, NULL, NULL, NULL, 1, 23, 0, 0, 0),
(208, 'Homeland Security, Department of', NULL, NULL, NULL, NULL, NULL, 1, 43, 0, 1, 0),
(209, 'Anoka County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(210, 'Dakota County', NULL, NULL, NULL, NULL, NULL, 1, 58, 100, 1, 0),
(211, 'Minnesota Supreme Court Judges', NULL, NULL, NULL, NULL, NULL, 1, 23, 0, 0, 0),
(212, 'Minnesota Tax Court Judges', NULL, NULL, NULL, NULL, NULL, 1, 23, 0, 0, 0),
(213, 'Minnesota Workers’ Compensation Judges', NULL, NULL, NULL, NULL, NULL, 1, 23, 0, 0, 0),
(214, 'Minnesota Workers’ Compensation Court of Appeals Judges', NULL, NULL, NULL, NULL, NULL, 1, 23, 0, 0, 0),
(215, 'County Cross-Reference', NULL, NULL, NULL, NULL, NULL, 1, 58, 10, 1, 0),
(216, 'Tenth Judicial District', NULL, NULL, NULL, NULL, NULL, 1, 23, 200, 1, 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_article_category`
--
ALTER TABLE `tbl_article_category`
  ADD CONSTRAINT `FK_category_state_id` FOREIGN KEY (`state_id`) REFERENCES `tbl_state` (`state_id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
