<?php

class AdminController extends Controller {

    /**
     * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
     * using two-column layout. See 'protected/views/layouts/column2.php'.
     */
      public $layout = '//layouts/column2';

  

    public function actionList() {
        if (!user()->isAdmin()){
            $this->accessDenied();
        } else {
                     $model= New Article_Category();
                    $request=Yii::app()->request;
                    $state=$request->getQuery('state');
                   
                    if ($state) { //state from get select dropdown and overwrite session
                        Yii::app()->session['state'] = $state;
                       
                    } else { //try loading from session
                       $state=Yii::app()->session['state'];
                    }
                    if ($state) { //save dropdown preselect
                         $model->state_id=$state;
                    } else { //set to 0 to avoid php notice
                        $state=0;
                    }
        
        }
       
        
        $this->render('//admin/index', array('model'=>$model,
            'state' => $state,
            
        ));
    }
   
}
?>
