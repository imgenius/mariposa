<?php

class CategoryController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view', 'list' ),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Article_Category;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Article_Category']))
		{
			$model->attributes=$_POST['Article_Category'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->cat_id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Article_Category']))
		{
			$model->attributes=$_POST['Article_Category'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->cat_id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

/**
	 * Lists sub-categories or articles for that category.
	 * @param integer $id the ID of the model to be listed
	 */
	public function actionList($id)
	{
		$model=$this->loadModel($id);
                $slug=$model->slug;
              
                if ($slug == 'feds') {
                    $this->render('feds',array(
			'model'=>$model,
                    ));
                } elseif ($slug == 'attorneys-a') {
                    //redirect to attorneys by alphabet view
                     $this->redirect(array('/attorney/alph',
			'state_id'=>$model->state_id
                    ));
                }  elseif ($slug == 'attorneys-c') {
                    //redirect to attorneys by alphabet view
                     $this->redirect(array('/attorney/county',
			'state_id'=>$model->state_id
                    ));
                } elseif ($slug == 'datecalc') {
                    //redirect to attorneys by alphabet view
                   $this->redirect(url('/custom/datecalc'));
                    
                } else {
		
		$this->render('list',array(
			'model'=>$model,
		));
		}
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$request = Yii::app()->request;
                $state = $request->getQuery('state');
                if ($state && ($state != 'All')) { //from get save into session
                  Yii::app()->session['state'] = $state;
                 } elseif ($state == 'All') {
                  Yii::app()->session['state'] = '';
                     
                 }    else {//try loading from session
                 $state = Yii::app()->session['state'];
                 }
                 if ($state) { //save dropdown preselect
                     $stateArray =State::model()->getStateArray();
                      if ($state && ($state != 'All')) {
                     $defaultType = $stateArray[$state];
                      } else {
                           $defaultType = '';
                      }
                } else { //set to 0 to avoid php notice
                    
                   //no default because we need to also be able to get the ones that don't have a state (federal)
                }
		
             // provide grid search functionality
            $filter = new Article_Category('search');
            $filter->attachBehavior('remember-filter', array(
               'class' => 'application.models.behaviors.ERememberFiltersBehavior',
           ));
            //$dataProvider=new CActiveDataProvider('Article');
            $dataProvider = new ActiveDataProvider('Article_Category', array(
           'criteria' => array(
                //'condition' => 'publish=1',
                'with' => array('state'),// => array(
                       // 'with' => 'priceBreakdown'
                   // )),
            ),
            'pagination' => array('pageSize' => 100),
                'sort' => array(
                    'defaultOrder' => 'priority ASC, t.name ASC'
                ),
            ));
            if ($state > 0) {
             $filter->state=$defaultType;
            }
            // search function has to return cdbcriteria instead of data provider
             $filter->filterDataProvider($dataProvider);
         
            //d($_GET);
		$this->render('index',array(
                    'dataProvider'=>$dataProvider,
                    'filter' => $filter,
                      'defaultType' => $defaultType,
		));
	}
	

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Article_Category('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Article_Category']))
			$model->attributes=$_GET['Article_Category'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer the ID of the model to be loaded
	 */
	public function loadModel($id)
	{
		$model=Article_Category::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CModel the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='article--category-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
