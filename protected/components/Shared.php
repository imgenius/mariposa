<?php

class Shared {

    private static $dbTimeZone;
    private static $userTimeZone = array();
    public static $dbTimeFormat = 'Y-m-d H:i:s';
    public static $dbDateFormat = 'Y-m-d';

    /**
     * File handler for debug function. It is used by d() shortcut as well.
     * @var file descriptor
     */
    public static $fh;
    
    /**
     * Timezones in USA
     * @var type 
     */
    static $timezones2 = array(
        //-10 => 'US - Hawaii',
        //-9 => 'US - Alaska',
        -8 => 'US - Pacific Time (Los Angeles)',
        -7 => 'US - Mountain Time (Denver)',
        -6 => 'US - Central Time',
        -5 => 'US - Eastern Time',
            //-4 => 'Canada - Atlantic Time'
    );
    // US TimeZones according to DateTime's official  "List of Supported Timezones" 
    public static $timezones = array(
        'AST' => array('zone' => 'America/Puerto_Rico', 'name' => 'Atlantic Standard Time', 'utcOffset' => -4),
        'EDT' => array('zone' => 'America/New_York', 'name' => 'Eastern Standard Time', 'utcOffset' => -5),
        'CDT' => array('zone' => 'America/Chicago', 'name' => 'Central Daylight Time', 'utcOffset' => -5),
        'MDT' => array('zone' => 'America/Boise', 'name' => 'Mountain Daylight Time', 'utcOffset' => -6),
        'MST' => array('zone' => 'America/Phoenix', 'name' => 'Mountain Standard Time', 'utcOffset' => - 7),
        //'PDT' => array('zone' => 'America/Los_Angeles', 'Pacific Daylight Time', 'utcOffset' => -7),
        'PST' => array('zone' => 'America/Los_Angeles', 'Pacific Standard Time', 'utcOffset' => -8),
    );

    /**
     * Instantiate user timezone object from session and return it. If there
     * is no timezone found, default timezone is used (Los Angeles).
     * @return DateTimeZone
     */
    public static function getUserTz() {
        $timezone = self::getUserTzName();
        if (!isset(self::$userTimeZone[$timezone])) {
            if (self::$dbTimeZone == null) {
                self::$dbTimeZone = new DateTimeZone('GMT');
            }
            
            self::$userTimeZone[$timezone] = new DateTimeZone($timezone);
        }
        return self::$userTimeZone[$timezone];
    }
    
    public static function getZipcodeTz($zipcode) {
        $timezone = app()->zip->getTimeZone($zipcode);
        if (!isset(self::$userTimeZone[$timezone])) {
            self::$userTimeZone[$timezone] = new DateTimeZone($timezone);
        }
        return self::$userTimeZone[$timezone];
    }

    /**
     * Return a name of TimeZone for the current user. Timezone is applied
     * when WebUser->setCurrentRole() function is executed.
     * @return string
     */
    public static function getUserTzName() {
        if (isset(app()->session['timezone'])) {
            $timezone = app()->session['timezone'];
        } else {
            $timezone = app()->params['defaultTimezone'];
        }
        return $timezone;
    }

    /**
     * Convert the time format to the database format
     */
    public static function timeDatabase($date) {
        $time = self::toTimestamp($date);
        $formatted = date('Y-m-d H:i:s', $time);
        return $formatted;
    }

    /**
     * Convert time from database (GMT) to given format and convert to 
     * user timezone.
     * @param string|int $date
     * @return string
     */
    public static function timeFromDatabaseTz($date, $toZipcode = null) {
        $userTz = self::getUserTz();

        // timestamps are not supported by DateTime object
        if (is_numeric($date)) {
            $date = date(self::$dbTimeFormat, $date);
        }
        $time = new DateTime($date, self::$dbTimeZone);
        $time->setTimezone($userTz);

        $formatted = $time->format(self::$dbTimeFormat);
        return $formatted;
    }

    public static function timeToDatabaseTz($date, $fromZipcode = null) {
        $userTz = self::getUserTz();

        // timestamps are not supported by DateTime object
        if (is_numeric($date)) {
            $date = date(self::$dbTimeFormat, $date);
        }
        $time = new DateTime($date, $userTz);
        $time->setTimezone(self::$dbTimeZone);

        $formatted = $time->format(self::$dbTimeFormat);
        return $formatted;
    }

    /**
     * Convert input date to Y-m-d format compatible with the database
     * @param type $date
     * @return type
     */
    public static function dateDatabase($date) {
        $time = self::toTimestamp($date);
        $formatted = date('Y-m-d', $time);
        return $formatted;
    }

    public static function dateDatabaseTz($date, $fromZipcode = null) {
        $userTz = self::getUserTz();
        if (is_numeric($date)) {
            $date = date(self::$dbTimeFormat, $date);
        }
        $time = new DateTime($date, $userTz);
        $time->setTimezone(self::$dbTimeZone);

        $formatted = $time->format(self::$dbDateFormat);
        return $formatted;
    }

    /**
     * Format TIME field coming from database in 24hrs HH:MM:SS format
     * @param type $date
     * @param $us if true, format is 12 hrs plus AM/PM, false returns 24 format
     */
    public static function formatTime($date, $usFormat = true) {
        $time = self::toTimestamp($date);
        if ($usFormat) {
            $formatted = date('h:i a', $time);
        } else {
            $formatted = date('H:i', $time);
        }
        return $formatted;
    }

    /**
     * day in a week, day month year
     * @param date $date
     * @return string
     */
    public static function formatDateShort($date) {
        $time = self::toTimestamp($date);
        $formatted = date('l, d F Y', $time);
        return $formatted;
    }
    
    public static function formatDate($date) {
        $time = self::toTimestamp($date);
        $formatted = date('d F Y', $time);
        return $formatted;
    }

    public static function formatDateLong($date) {
        $time = self::toTimestamp($date);
        $formatted = date('l, d F Y h:i a', $time);
        return $formatted;
    }
    
    /**
     * Convert time input compatible with database TIME field
     * @param type $time
     */
    public static function timeTo24hrs($time) {
        if (strtolower(strstr($time, 'am'))) {
            // no change
            return preg_replace('/[^\d:]/', '', $time);
        } else if (strtolower(strstr($time, 'pm'))) {
            // add 12 hrs
            $formatted = preg_replace('/[^\d:]/', '', $time);
            list($hr, $min) = explode(":", $formatted);
            return ($hr + 12) % 24 . ":" . $min;
        } else {
            // 24 hrs already
            return $time;
        }
    }

    /**
     * Converts everything into the timestamp
     * @param <mixed> $date
     * @return <timestamp>
     */
    public static function toTimestamp($date) {
        if (is_int($date)) {
            return $date;
        }
        if (($time = strtotime($date)) !== false) {
            return $time;
        }

        return false;
    }

    /**
     * Get time in database format. Useful for update / create timestamps
     * @return string
     */
    public static function getTimestamp() {
        return self::timeDatabase(time());
    }

    /**
     * Get timestamp in database format in UTC timezone
     * @return type
     */
    public static function getDbTimestamp() {
        return self::timeDatabase(time());
    }

    /**
     * Generate 8 letter long random password.
     */
    static function generateRandomPassword($length = 7) {

        $chars = "abcdefghijkmnopqrstuvwxyzABCDEFGHIJKLMNPQRSTUVWXZ023456789";
        srand((double) microtime() * 1000000);
        $i = 0;
        $pass = '';

        while ($i <= $length) {
            $num = mt_rand() % strlen($chars);
            $tmp = substr($chars, $num, 1);
            $pass = $pass . $tmp;
            $i++;
        }

        return $pass;
    }

    /**
     * Produces a little bit better passwords you can remember
     * The lenght is 8 characters, contains two digits and one capital letter
     * @return <type>
     */
    static function generateMnemonicPassword() {
        $numlen = 1;
        $length = 6;
        $output = '';
        $vowels = 'aeioe';
        $consonants = 'bcdfghklmnpqrstvwxzy';
        srand((double) microtime() * 1000000);

        for ($i = 0; $i < $length / 2; $i++) {
            $output .= $vowels[rand() % strlen($vowels)];
            $output .= $consonants[rand() % strlen($consonants)];
        }

        // put there one capital letter
        $rand = rand() % strlen($length - 1);
        $output[$rand] = strtoupper($output[$rand]);

        $pos = rand(2, $length - 2 - $numlen);
        return substr($output, 0, $pos) . rand(10, 99) . substr($output, $pos);
    }

    /**
     * Get numeric representation of a phone number, which we store in the
     * database.
     * @param type $string
     * @return boolean|null
     */
    static function phoneToDatabase($string) {
        $value = str_replace(" ", '', $string);

        // allow null values
        if (strlen($value) == 0)
            return false;

        $formats = array('###-###-####', '####-###-###', '#########',
            '(###)###-###', '####-####-####', '(###)###-####', '#(###)###-####',
            '##-###-####-####', '####-####', '###-###-###',
            '#####-###-###', '##########', '+############', '00############');
        $format = preg_replace("/[0-9]/", "#", $value);
        $format = str_replace(' ', '-', $format);
        if (in_array($format, $formats)) {

            $value = str_replace("-", '', $value);
            $value = str_replace("(", '', $value);
            $value = str_replace(")", '', $value);
            // update the value
            return $value;
        }

        return null;
    }

    /**
     * Format phone number to human readable form.
     */
    public static function formatPhone($number) {
        $number = preg_replace("/[^0-9]/", "", $number); // Strip non-numbers
        if (strlen($number) == 10) {
            return "(" . substr($number, 0, 3) . ") " . substr($number, 3, 3) . "-" . substr($number, 6);
        }
        if (strlen($number) >= 11) {
            return "+" . substr($number, 0, 1) . " (" . substr($number, 1, 3) . ") " . substr($number, 4, 3) . "-" . substr($number, 7);
        }
        return $number;
    }
    
    /**
     * Check that DNS MX record exists for a given domain
     * and email format is valid.
     */
    public static function isEmailValid($email) {
        if (!preg_match("/^[_a-zA-Z0-9-_]+(\.[_a-zA-Z0-9-_]+)*@[a-zA-Z0-9-_]+(\.[a-zA-Z0-9-_]+)*(\.[a-zA-Z]{2,3})$/", $email)) {
            return false;
        }
        list($userid, $domain) = explode("@", $email);

        // most common domains might speed up lookup
        $validDomains = array(
            "gmail.com" => true,
            "freshrealm.co" => true,
            "yahoo.com" => true,
            "hotmail.com" => true,
            "aol.com" => true,
            "msn.com" => true
        );
        if (isset($validDomains[$domain])) {
            return true;
        }

        // check its DNS record
        if (checkdnsrr($domain, "MX")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get a unique identifier, which can be used as an id.
     * @author http://www.php.net/manual/en/function.uniqid.php#94959
     * @return type
     */
    public static function getUuid() {
        return sprintf('%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
                // 32 bits for "time_low"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff),
                // 16 bits for "time_mid"
                mt_rand(0, 0xffff),
                // 16 bits for "time_hi_and_version",
                // four most significant bits holds version number 4
                mt_rand(0, 0x0fff) | 0x4000,
                // 16 bits, 8 bits for "clk_seq_hi_res",
                // 8 bits for "clk_seq_low",
                // two most significant bits holds zero and one for variant DCE1.1
                mt_rand(0, 0x3fff) | 0x8000,
                // 48 bits for "node"
                mt_rand(0, 0xffff), mt_rand(0, 0xffff), mt_rand(0, 0xffff)
        );
    }

    /**
     * Converts table name into class name in yii
     * @param type $word
     * @return type
     */
    public static function camelize($word) {
        return ucfirst(preg_replace('/(^|_)([a-z])/e', 'strtoupper("\\2")', $word));
    }
    
    /**
     * Returns lookup array for array of models obtained from findAllBy function.
     * The key of new array is models PK.
     * Works only for models with single PK (id column)
     * @param type $arList
     */
    public static function getModelsByPk($arList) {
        $output = array();
        foreach ($arList as $model) {
            $output[$model->id] = $model;
        }
        return $output;
    }

    /**
     * Extract positive number from any given string
     * @param type $input
     * @return array
     */
    public static function extractNumber($input) {
        $matches = array();
        preg_match_all('/[0-9\.]+/', $input, $matches);
        d($matches);
        if (isset($matches[0][0])) {
            return $matches[0][0];
        }
        return null;
    }

    public static function pluralizeAndConvertToLower($string){
        return strtolower($string . "s");
    }

    /**
     * Pass in enum type data from database and return a readable format for reports
     * @param string $string
     * @return string
     */
    public static function convertDataToReadable($string) {
        $string = str_replace('_',' ', $string);
        return ucwords($string);
    } 
}

?>
