<?php

/**
 * Display simple search bar for grid / list views and switching between views
 */
class SearchBar extends CWidget {

    public $displaySearchBox = true;
    public $displayAdvancedSearch = false;
    public $searchPlaceholder = 'search';
    public $appendHtml = '';
    public $advancedSearchForm;
    public $listId = null;
    public $gridId = null;

    /**
     * Required model to init search
     * @var FrActiveRecord 
     */
    public $searchModel;

    /**
     * Grid is a table, list is the tile view.
     * @var type 
     */
    public $showGridSwitch = false;

    public function init() {
        if ($this->searchModel == null) {
            throw new CException("Search model in search bar is a required attribute.");
        }
        if ($this->listId == null && $this->gridId == null) {
            throw new CException("Specify either grid id or list id. It is an html id attribute of related cgridview or clistview");
        }
        echo "<div class='well search-bar'>";
        //CActiveForm::
        $form = $this->beginWidget('widgets.FrActiveForm', array(
            'id' => 'search-form',
            'type' => 'inline',
            'method' => 'get',
            'htmlOptions' => array(
                'class' => 'span4 pull-left'
            )
                //'enableAjaxValidation' => true,
                //'focus' => array($this->searchModel, 'global_search'),
        ));
        echo CHtml::activeTextField($this->searchModel, 'globalSearch', array('class' => 'span4 globalSearch', 'placeholder' => $this->searchPlaceholder));
        $this->endWidget();
        if ($this->appendHtml != null){
            echo "<div class='pull-right'>" . $this->appendHtml . '</div>';
        }
        // icon is moved to the left using css
        /*
        echo "<span class='icon-search2 search-icon' />";
        if (!empty($this->showGridSwitch)) {
            echo '<div class="grid-switch pull-right">';
            if(!isset($_GET['type'])) {
                echo '<a href="' . absurl(app()->request->getPathInfo()) . '?type=table"><span class="grid-switch-grid icon-paragraph-justify active"></span></a>';
                echo '<a href="' . absurl(app()->request->getPathInfo()) . '?type=tiles"><span class="grid-switch-list icon-grid"></span></a>';
            } else if ($_GET['type'] == "table") {
                echo '<a href="' . absurl(app()->request->getPathInfo()) . '?type=table"><span class="grid-switch-grid icon-paragraph-justify active"></span></a>';
                echo '<a href="' . absurl(app()->request->getPathInfo()) . '?type=tiles"><span class="grid-switch-list icon-grid"></span></a>';
            } else {
                echo '<a href="' . absurl(app()->request->getPathInfo()) . '?type=table"><span class="grid-switch-grid icon-paragraph-justify"></span></a>';
                echo '<a href="' . absurl(app()->request->getPathInfo()) . '?type=tiles"><span class="grid-switch-list icon-grid active"></span></a>';
            }
            echo '</div>';
        }*/
        echo "</div>";
        $this->registerSearchScripts();
    }

    private function registerSearchScripts() {
        cs()->registerPackage('jquery.ui');
        cs()->registerScript('searchbarwidget', '
$.widget("ui.onDelayedKeyup", {

    _init : function() {
        var self = this;
        $(this.element).keyup(function() {
            if(typeof(window["inputTimeout"]) != "undefined"){
                window.clearTimeout(inputTimeout);
            }  
            var handler = self.options.handler;
            window["inputTimeout"] = window.setTimeout(function() {
                handler.call(self.element) }, self.options.delay);
        });
    },
    options: {
        handler: $.noop(),
        delay: 500
    }

});

$("input.globalSearch").onDelayedKeyup({
        handler: function() {
            ' . ($this->gridId != null ? '
            $.fn.yiiGridView.update("' . $this->gridId . '", {
                data: $("#search-form").serialize()
            });
            return false;
            ' : '$.fn.yiiListView.update("' . $this->listId . '", {
                data: $("#search-form").serialize()
            });
            return false;') . '

        }
    });
    
');
    }

}
