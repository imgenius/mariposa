<?php

/**
 * FrInputHorizontal class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets.input
 */
Yii::import('bootstrap.widgets.input.BootInput');

/**
 * Bootstrap horizontal form input widget.
 * @since 0.9.8
 */
class FrInputHorizontal extends FrInput {

    //private $rowClass = 'form-row row';
    //private $formElementClass = 'span7';

    /**
     * Runs the widget.
     */
    /* public function run() {
      echo CHtml::openTag('div', array('class' => 'control-group ' . $this->getContainerCssClass()));
      parent::run();
      echo '</div>';
      } */

    /**
     * Returns the label for this block.
     * @return string the label
     */
    /* protected function getLabel() {
      if (isset($this->labelOptions['class']))
      $this->labelOptions['class'] .= ' control-label';
      else
      $this->labelOptions['class'] = 'control-label';

      return parent::getLabel();
      } */

    /**
     * Renders a checkbox.
     * @return string the rendered content
     */
    protected function checkBox() {
        echo "<div class='{$this->rowClass}'>";

        // not sure why the label is not loaded by default.
        // TODO: fix the label
        $attribute = $this->attribute;
        $this->label = $this->model->getAttributeLabel($attribute);
        echo $this->getLabel();

        echo "<div class='{$this->formElementClass}'>";
        echo $this->form->checkBox($this->model, $attribute, $this->htmlOptions) . PHP_EOL;
        //echo '<label class="checkbox" for="' . $this->getAttributeId($attribute) . '">';
        //echo $this->model->getAttributeLabel($attribute);

        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Renders a list of checkboxes.
     * @return string the rendered content
     */
    protected function checkBoxList() {
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel();
        echo "<div class='{$this->formElementClass}'>";
        echo $this->form->checkBoxList($this->model, $this->attribute, $this->data, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Renders a list of inline checkboxes.
     * @return string the rendered content
     */
    protected function checkBoxListInline() {
        echo "<div class='{$this->rowClass}'>";
        $this->htmlOptions['inline'] = true;
        $this->checkBoxList();
        echo '</div>';
    }

    /**
     * Renders a list of checkboxes using Button Groups.
     * @return string the rendered content
     */
    protected function checkBoxGroupsList() {
        if (isset($this->htmlOptions['for']) && !empty($this->htmlOptions['for'])) {
            $label_for = $this->htmlOptions['for'];
            unset($this->htmlOptions['for']);
        } else if (isset($this->data) && !empty($this->data)) {
            $label_for = CHtml::getIdByName(
                            get_class($this->model) . '[' . $this->attribute . '][' . key($this->data) . ']'
            );
        }

        if (isset($label_for)) {
            $this->labelOptions = array('for' => $label_for);
        }

        $this->htmlOptions['class'] = 'pull-left';

        echo $this->getLabel();
        echo '<div class="controls">';
        echo $this->form->checkBoxGroupsList($this->model, $this->attribute, $this->data, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a drop down list (select).
     * @return string the rendered content
     */
    protected function dropDownList() {
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel();
        echo "<div class='{$this->formElementClass}'>";
        echo $this->form->dropDownList($this->model, $this->attribute, $this->data, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Renders a file field.
     * @return string the rendered content
     */
    protected function fileField() {
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel();
        echo "<div class='{$this->formElementClass}'>";
        echo $this->form->fileField($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Renders a password field.
     * @return string the rendered content
     */
    protected function passwordField() {
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel();
        echo "<div class='{$this->formElementClass}'>";
        echo $this->getPrepend();
        echo $this->form->passwordField($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo $this->getAppend();
        echo '</div></div>';
    }

    /**
     * Renders a radio button.
     * @return string the rendered content
     */
    protected function radioButton() {
        echo "<div class='{$this->rowClass}'>";
        $attribute = $this->attribute;
        echo "<div class='{$this->formElementClass}'>";
        echo '<label class="radio" for="' . $this->getAttributeId($attribute) . '">';
        echo $this->form->radioButton($this->model, $attribute, $this->htmlOptions) . PHP_EOL;
        echo $this->model->getAttributeLabel($attribute);
        echo $this->getError() . $this->getHint();
        echo '</label></div></div>';
    }

    /**
     * Renders a list of radio buttons.
     * @return string the rendered content
     */
    protected function radioButtonList() {
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel();
        echo "<div class='{$this->formElementClass}'>";
        echo $this->form->radioButtonList($this->model, $this->attribute, $this->data, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Renders a list of inline radio buttons.
     * @return string the rendered content
     */
    protected function radioButtonListInline() {
        echo "<div class='{$this->rowClass}'>";
        $this->htmlOptions['inline'] = true;
        $this->radioButtonList();
        echo '</div>';
    }

    /**
     * Renders a list of radio buttons using Button Groups.
     * @return string the rendered content
     */
    protected function radioButtonGroupsList() {
        if (isset($this->htmlOptions['for']) && !empty($this->htmlOptions['for'])) {
            $label_for = $this->htmlOptions['for'];
            unset($this->htmlOptions['for']);
        } else if (isset($this->data) && !empty($this->data)) {
            $label_for = CHtml::getIdByName(
                            get_class($this->model) . '[' . $this->attribute . '][' . key($this->data) . ']'
            );
        }

        if (isset($label_for)) {
            $this->labelOptions = array('for' => $label_for);
        }

        $this->htmlOptions['class'] = 'pull-left';

        echo $this->getLabel();
        echo '<div class="controls">';
        echo $this->form->radioButtonGroupsList($this->model, $this->attribute, $this->data, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a textarea.
     * @return string the rendered content
     */
    protected function textArea() {
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel();
        echo "<div class='{$this->formElementClass}'>";
        echo $this->getPrepend();
        echo $this->form->textArea($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo $this->getAppend();
        echo '</div>';
        echo '</div>';
    }

    /**
     * Renders a text field.
     * @return string the rendered content
     */
    protected function textField() {
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel() . $this->getHint();
        echo "<div class='{$this->formElementClass}'>";
        echo $this->getPrepend();
        echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getError();
        echo $this->getAppend();
        echo '</div>';
        echo '</div>';
    }

    /**
     * Renders a masked text field.
     * @return string the rendered content
     */
    protected function maskedTextField() {
        echo $this->getLabel();
        echo '<div class="controls">';
        echo $this->getPrepend();
        echo $this->form->maskedTextField($this->model, $this->attribute, $this->data, $this->htmlOptions);
        echo $this->getAppend();
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a CAPTCHA.
     * @return string the rendered content
     */
    protected function captcha() {
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel();
        echo "<div class='{$this->formElementClass}'><div class='captcha'>";
        echo '<div class="widget">' . $this->widget('CCaptcha', $this->captchaOptions, true) . '</div>';
        echo $this->form->textField($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getError() . $this->getHint();
        echo '</div></div></div>';
    }

    /**
     * Renders an uneditable field.
     * @return string the rendered content
     */
    protected function uneditableField() {
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel();
        echo "<div class='{$this->formElementClass}'>";
        echo CHtml::tag('span', $this->htmlOptions, $this->model->{$this->attribute});
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * This function is radically different in new bootstrap
     * Render date picker calendar text box field
      protected function datepickerField() {
      echo "<div class='{$this->rowClass}'>";
      // register extra scripts
      $assetsPath = Yii::getPathOfAlias('application.components.assets');
      $assetsUrl = Yii::app()->assetManager->publish($assetsPath, false, -1, YII_DEBUG);
      if (!cs()->isScriptFileRegistered($assetsUrl . '/js/bootstrap-datepicker.js')) {
      cs()->registerScriptFile($assetsUrl . '/js/bootstrap-datepicker.js');
      cs()->registerCssFile($assetsUrl . '/css/bootstrap-datepicker.css');
      }
      $extraOptions = array(
      'data-tbdatepicker' => 'datepicker',
      'class' => 'span'
      );
      echo $this->getLabel();
      echo "<div class='{$this->formElementClass}'>";
      echo $this->getPrepend();
      echo $this->form->textField($this->model, $this->attribute, CMap::mergeArray($this->htmlOptions, $extraOptions));
      echo $this->getError() . $this->getHint();
      echo $this->getAppend();
      echo '</div></div>';
      }
     */

    /**
     * Renders a datepicker field.
     * @return string the rendered content
     * @author antonio ramirez <antonio@clevertech.biz>
     */
    protected function datepickerField() {
        if (isset($this->htmlOptions['options'])) {
            $options = $this->htmlOptions['options'];
            unset($this->htmlOptions['options']);
        }

        if (isset($this->htmlOptions['events'])) {
            $events = $this->htmlOptions['events'];
            unset($this->htmlOptions['events']);
        }
        
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel();
        echo "<div class='{$this->formElementClass}'>";
        $this->widget(
                'bootstrap.widgets.TbDatePicker', array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'options' => isset($options) ? $options : array(),
            'events' => isset($events) ? $events : array(),
            'htmlOptions' => $this->htmlOptions,
                )
        );
        echo $this->getError() . $this->getHint();
        echo '</div></div>';
    }

    /**
     * Renders a colorpicker field.
     * @return string the rendered content
     * @author antonio ramirez <antonio@clevertech.biz>
     */
    protected function colorpickerField() {
        $format = 'hex';
        if (isset($this->htmlOptions['format'])) {
            $format = $this->htmlOptions['format'];
            unset($this->htmlOptions['format']);
        }

        if (isset($this->htmlOptions['events'])) {
            $events = $this->htmlOptions['events'];
            unset($this->htmlOptions['events']);
        }

        echo $this->getLabel();
        echo '<div class="controls">';
        echo $this->getPrepend();
        $this->widget(
                'bootstrap.widgets.TbColorPicker', array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'format' => $format,
            'events' => isset($events) ? $events : array(),
            'htmlOptions' => $this->htmlOptions,
                )
        );
        echo $this->getAppend();
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a redactor.
     * @return string the rendered content
     */
    protected function redactorJs() {
        if (isset($this->htmlOptions['options'])) {
            $options = $this->htmlOptions['options'];
            unset($this->htmlOptions['options']);
        }
        if (isset($this->htmlOptions['width'])) {
            $width = $this->htmlOptions['width'];
            unset($this->htmlOptions['width']);
        }
        if (isset($this->htmlOptions['height'])) {
            $height = $this->htmlOptions['height'];
            unset($this->htmlOptions['height']);
        }
        echo $this->getLabel();
        echo '<div class="controls">';
        $this->widget(
                'bootstrap.widgets.TbRedactorJs', array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'editorOptions' => isset($options) ? $options : array(),
            'width' => isset($width) ? $width : '100%',
            'height' => isset($height) ? $height : '400px',
            'htmlOptions' => $this->htmlOptions
                )
        );
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a Markdown Editor.
     * @return string the rendered content
     */
    protected function markdownEditorJs() {

        if (isset($this->htmlOptions['width'])) {
            $width = $this->htmlOptions['width'];
            unset($this->htmlOptions['width']);
        }
        if (isset($this->htmlOptions['height'])) {
            $height = $this->htmlOptions['height'];
            unset($this->htmlOptions['height']);
        }
        echo $this->getLabel();
        echo '<div class="controls">';
        echo '<div class="wmd-panel">';
        echo '<div id="wmd-button-bar" class="btn-toolbar"></div>';
        $this->widget(
                'bootstrap.widgets.TbMarkdownEditorJs', array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'width' => isset($width) ? $width : '100%',
            'height' => isset($height) ? $height : '400px',
            'htmlOptions' => $this->htmlOptions
                )
        );
        echo $this->getError() . $this->getHint();
        echo '<div id="wmd-preview" class="wmd-panel wmd-preview" style="width:' . (isset($width) ? $width : '100%') . '"></div>';
        echo '</div>'; // wmd-panel
        echo '</div>'; // controls
    }

    /**
     * Renders Bootstrap wysihtml5 editor.
     * @return mixed|void
     */
    protected function html5Editor() {
        if (isset($this->htmlOptions['options'])) {
            $options = $this->htmlOptions['options'];
            unset($this->htmlOptions['options']);
        }
        if (isset($this->htmlOptions['width'])) {
            $width = $this->htmlOptions['width'];
            unset($this->htmlOptions['width']);
        }
        if (isset($this->htmlOptions['height'])) {
            $height = $this->htmlOptions['height'];
            unset($this->htmlOptions['height']);
        }
        echo $this->getLabel();
        echo '<div class="controls">';
        $this->widget(
                'bootstrap.widgets.TbHtml5Editor', array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'editorOptions' => isset($options) ? $options : array(),
            'width' => isset($width) ? $width : '100%',
            'height' => isset($height) ? $height : '400px',
            'htmlOptions' => $this->htmlOptions
                )
        );
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a ckEditor.
     * @return string the rendered content
     * @author antonio ramirez <antonio@clevertech.biz>
     */
    protected function ckEditor() {
        if (isset($this->htmlOptions['options'])) {
            $options = $this->htmlOptions['options'];
            unset($this->htmlOptions['options']);
        }

        echo $this->getLabel();
        echo '<div class="controls">';
        $this->widget(
                'bootstrap.widgets.TbCKEditor', array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'editorOptions' => isset($options) ? $options : array(),
            'htmlOptions' => $this->htmlOptions
                )
        );
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a daterange field.
     * @return string the rendered content
     * @author antonio ramirez <antonio@clevertech.biz>
     */
    protected function dateRangeField() {
        if (isset($this->htmlOptions['options'])) {
            $options = $this->htmlOptions['options'];
            unset($this->htmlOptions['options']);
        }

        if (isset($options['callback'])) {
            $callback = $options['callback'];
            unset($options['callback']);
        }

        echo $this->getLabel();
        echo '<div class="controls">';
        echo $this->getPrepend();
        $this->widget(
                'bootstrap.widgets.TbDateRangePicker', array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'options' => isset($options) ? $options : array(),
            'callback' => isset($callback) ? $callback : array(),
            'htmlOptions' => $this->htmlOptions,
                )
        );
        echo $this->getAppend();
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a timepicker field.
     * @return string the rendered content
     * @author Sergii Gamaiunov <hello@webkadabra.com>
     */
    protected function timepickerField() {
        if (isset($this->htmlOptions['options'])) {
            $options = $this->htmlOptions['options'];
            unset($this->htmlOptions['options']);
        }

        if (isset($this->htmlOptions['events'])) {
            $events = $this->htmlOptions['events'];
            unset($this->htmlOptions['events']);
        }

        echo $this->getLabel();
        echo '<div class="controls bootstrap-timepicker">';
        echo $this->getPrepend();
        $this->widget(
                'bootstrap.widgets.TbTimePicker', array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'options' => isset($options) ? $options : array(),
            'events' => isset($events) ? $events : array(),
            'htmlOptions' => $this->htmlOptions,
            'form' => $this->form
                )
        );
        echo $this->getAppend();
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a select2Field
     * @return mixed|void
     */
    protected function select2Field() {
        if (isset($this->htmlOptions['options'])) {
            $options = $this->htmlOptions['options'];
            unset($this->htmlOptions['options']);
        }

        if (isset($this->htmlOptions['events'])) {
            $events = $this->htmlOptions['events'];
            unset($this->htmlOptions['events']);
        }

        if (isset($this->htmlOptions['data'])) {
            $data = $this->htmlOptions['data'];
            unset($this->htmlOptions['data']);
        }

        if (isset($this->htmlOptions['asDropDownList'])) {
            $asDropDownList = $this->htmlOptions['asDropDownList'];
            unset($this->htmlOptions['asDropDownList']);
        }

        echo $this->getLabel();
        echo '<div class="controls">';
        echo $this->getPrepend();
        $this->widget(
                'bootstrap.widgets.TbSelect2', array(
            'model' => $this->model,
            'attribute' => $this->attribute,
            'options' => isset($options) ? $options : array(),
            'events' => isset($events) ? $events : array(),
            'data' => isset($data) ? $data : array(),
            'asDropDownList' => isset($asDropDownList) ? $asDropDownList : true,
            'htmlOptions' => $this->htmlOptions,
            'form' => $this->form
                )
        );
        echo $this->getAppend();
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a typeAhead field.
     * @return string the rendered content
     */
    protected function typeAheadField() {
        echo $this->getLabel();
        echo '<div class="controls">';
        echo $this->getPrepend();
        echo $this->form->typeAheadField($this->model, $this->attribute, $this->data, $this->htmlOptions);
        echo $this->getAppend();
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Renders a number field.
     * @return string the rendered content
     */
    protected function numberField() {
        echo $this->getLabel();
        echo '<div class="controls">';
        echo $this->getPrepend();
        echo $this->form->numberField($this->model, $this->attribute, $this->htmlOptions);
        echo $this->getAppend();
        echo $this->getError() . $this->getHint();
        echo '</div>';
    }

    /**
     * Render two buttons (select from template and upload file) fields with
     * image preview
     */
    protected function imageField() {
        echo "<div class='{$this->rowClass}'>";
        // TODO: make it  a configuration option
        if ($this->label == null) {
            $this->label = 'Upload Image';
        }
        echo $this->getLabel() . $this->getHint();
        echo "<div class='{$this->formElementClass}'>";
        echo $this->getPrepend();
        Yii::app()->controller->widget('ext.imageupload.ImageUploadWidget', array(
            'model' => $this->model,
            'layout' => isset($this->imageOptions['layout']) ? $this->imageOptions['layout'] : null,
            'form' => $this->form,
            // javascript executed after upload and resize is done
            //'afterFinish' => isset($this->imageOptions['afterFinish']) ? $this->imageOptions['afterFinish'] : null,
            // javascript executed after first upload is done
            'afterUpload' => isset($this->imageOptions['afterUpload']) ? $this->imageOptions['afterUpload'] : null,
            // what images are loaded as template ones
            //'template' => isset($this->imageOptions['template']) ? $this->imageOptions['template'] : null,
            'configuration' => isset($this->imageOptions['configuration']) ? $this->imageOptions['configuration'] : 'default'));
        echo $this->getError();
        echo $this->getAppend();
        echo '</div>';
        echo '</div>';
    }

    /**
     * Render month, day, year drop down fields in one row
     */
    public function dateDropDownField() {

        $monthMap = array(
            1 => 'Jan',
            2 => 'Feb',
            3 => 'Mar',
            4 => 'Apr',
            5 => 'May',
            6 => 'Jun',
            7 => 'Jul',
            8 => 'Aug',
            9 => 'Sep',
            10 => 'Oct',
            11 => 'Nov',
            12 => 'Dec'
        );

        $years = array('' => 'year');
        $months = array('' => 'month');
        $days = array('' => 'day');

        $yearNow = date('Y') + 0;

        for ($i = $yearNow - 17; $i > $yearNow - 100; $i--) {
            $years[$i] = $i;
        }

        for ($i = 1; $i <= 31; $i++) {
            $days[$i] = $i;
        }

        for ($i = 1; $i <= 12; $i++) {
            $months[$i] = $i . ' - ' . $monthMap[$i];
        }

        $prefix = get_class($this->model) . '_' . $this->attribute;
        $value = Shared::toTimestamp($this->model->{$this->attribute});

        // update hidden field when value of any drop down changes
        cs()->registerScript($prefix . '_dateselect', '
            function ' . $prefix . '_frDateDropdown(){
                var year = $("#' . $prefix . '_year").val();
                var month = $("#' . $prefix . '_month").val() || 1;
                var day = $("#' . $prefix . '_day").val() || 1;
                console.log(year, month, day);
                if (year) {
                    $("#' . $prefix . '").val(year + "-" + month + "-" + day);
                }
            }
            
            $("#' . $prefix . '_year").change(function(e){' . $prefix . '_frDateDropdown();});
            $("#' . $prefix . '_month").change(function(e){' . $prefix . '_frDateDropdown();});
            $("#' . $prefix . '_day").change(function(e){' . $prefix . '_frDateDropdown();});
        ');

        // render the element
        $this->htmlOptions['class'] = $this->rowClass;
        $content = $this->getLabel();
        $content .= "<div class='{$this->formElementClass}'>";

        $content .= CHtml::dropDownList($prefix . '_year', $value ? date('Y', $value) : '', $years, array('style' => 'width: 30%;'));
        $content .= CHtml::dropDownList($prefix . '_month', $value ? date('m', $value) : '', $months, array('style' => 'width: 30%; margin-left: 5%;'));
        $content .= CHtml::dropDownList($prefix . '_day', $value ? date('d', $value) : '', $days, array('style' => 'width: 30%; margin-left: 5%;'));
        $content .= CHtml::activeHiddenField($this->model, $this->attribute);

        $content .= $this->getError() . $this->getHint();
        $content .= '</div>';
        echo CHtml::tag('div', $this->htmlOptions, $content);
    }

    /**
     * Render custom html element together with label for this field. Use this
     * option if you need to customize the input row.
     */
    public function customField() {
        echo "<div class='$this->rowClass'>";
        echo $this->getLabel();
        //echo '<div class="span2"><label>' . $this->data['label'] . '</label></div>';
        //$content .= '<div class="form-element">';
        $this->htmlOptions['class'] = $this->formElementClass;
        $content = $this->data['element'];
        $content .= $this->getError() . $this->getHint();
        //$content .= '</div>';
        echo CHtml::tag('div', $this->htmlOptions, $content);
        //echo "<div class='clearfix'></div>";
        echo "</div>";
    }

    /**
     * Renders a toogle button
     * @return string the rendered content
     */
    protected function toggleButton() {
        // widget configuration is set on htmlOptions['options']
        $options = array(
            'model' => $this->model,
            'attribute' => $this->attribute
        );
        if (isset($this->htmlOptions['options'])) {
            $options = CMap::mergeArray($options, $this->htmlOptions['options']);
            unset($this->htmlOptions['options']);
        }
        $options['htmlOptions'] = $this->htmlOptions;
        echo "<div class='{$this->rowClass}'>";
        echo $this->getLabel() . $this->getHint();
        echo "<div class='{$this->formElementClass}'>";
        echo $this->getPrepend();
        $this->widget('bootstrap.widgets.TbToggleButton', $options);
        echo $this->getError();
        echo $this->getAppend();
        echo '</div>';
        echo '</div>';
    }

}
