<?php

/**
 * FrInput class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 * @package bootstrap.widgets.input
 */
Yii::import('bootstrap.widgets.input.TbInput');

/**
 * Bootstrap input widget.
 * Used for rendering inputs according to Bootstrap standards.
 */
abstract class FrInput extends TbInput {
    // The different input types.
    // these could be replaced by regular CHtml elements

    const TYPE_DATEPICKER = 'datepicker';
    const TYPE_IMAGE = 'imagefield';
    const TYPE_CUSTOM = 'custom';
    const TYPE_DATEDROPDOWN = 'datedropdown';

    public $hintInline;
    protected $imageOptions;

    protected $rowClass = 'form-row row';
    protected $formElementClass = 'span7';
    protected $labelRowClass = 'span2';
    
    public function init() {
        parent::init();
        $this->formElementClass = $this->form->fieldRowClass;
        $this->labelRowClass = $this->form->labelRowClass;
    }

        /**
     * Runs the widget.
     * @throws CException if the widget type is invalid.
     */
    public function run() {
        try {
            parent::run();
            return null;
        } catch (CException $exc) {
            //echo $exc->getTraceAsString();
            if (!strstr($exc->getMessage(), 'TbInput')){
                d("pass it forward");
                // the exception is not widget related
                throw $exc;
            }
        }

        switch ($this->type) {

            case self::TYPE_IMAGE:
                $this->imageField();
                break;

            case self::TYPE_CUSTOM:
                $this->customField();
                break;

            case self::TYPE_DATEDROPDOWN:
                $this->dateDropDownField();
                break;

            case self::TYPE_DATERANGEPICKER:
                $this->dateRangeField();
                break;

            default:
                throw new CException(__CLASS__ . ': Failed to run widget! Type ' . $this->type . ' is invalid.');
        }
    }

    protected function getLabel() {
        $questionMark = "";
        if (isset($this->hintText)) {
            $questionMark = "<div title='" . CHtml::encode($this->hintText) . "' class='fr-tooltip'></div>";
        }
        if ($this->label !== null) {
            $label = "<label>" . $this->label . $questionMark . "</label>";
        } else
        if (!in_array($this->type, array('checkbox', 'radio')) && $this->hasModel()) {
            if (isset($this->hintText)) {

                // we have to include a question mark in the label
                $this->labelOptions['label'] = $this->model->getAttributeLabel($this->attribute) . $questionMark;
                $label = $this->form->labelEx($this->model, $this->attribute, $this->labelOptions);
            } else {
                $label = $this->form->labelEx($this->model, $this->attribute, $this->labelOptions);
            }
        } else {
            $label = '';
        }
        return "<div class='$this->labelRowClass'>$label</div>";
    }

    /**
     * Returns the prepend element for the input.
     * @return string the element
     */
    protected function getPrepend() {
        if ($this->hasAddOn()) {
            $htmlOptions = $this->prependOptions;

            if (isset($htmlOptions['class']))
                $htmlOptions['class'] .= ' add-on';
            else
                $htmlOptions['class'] = 'add-on';

            ob_start();
            echo '<div class="' . $this->getAddonCssClass() . '">';
            if (isset($this->prependText))
                echo CHtml::tag('span', $htmlOptions, $this->prependText);

            return ob_get_clean();
        }
        else
            return '<div class="clearfix">';
    }

    /**
     * Returns the append element for the input.
     * @return string the element
     */
    protected function getAppend() {
        $hint = $this->getHintInline();
        if ($this->hasAddOn()) {
            $htmlOptions = $this->appendOptions;

            if (isset($htmlOptions['class']))
                $htmlOptions['class'] .= ' add-on';
            else
                $htmlOptions['class'] = 'add-on';

            ob_start();
            if (isset($this->appendText))
                echo CHtml::tag('span', $htmlOptions, $this->appendText);

            echo $hint . '</div>';
            return ob_get_clean();
        } else {
            return $hint . '</div>';
        }
    }

    /**
     * Rerutrns hint for a popup question mark
     * @return string
     */
    /* protected function getHint() {
      if (isset($this->hintText)) {
      $htmlOptions = $this->hintOptions;
      if (isset($htmlOptions['class']))
      $htmlOptions['class'] .= ' help-block';
      else
      $htmlOptions['class'] = 'help-block';
      return '<a class="fr-toolTip" rel="tooltip" title="' . $this->hintText . '"></a>';
      } else {
      return '';
      }
      } */

    /**
     * Rerutrns hint for a popup question mark
     * @return string
     */
    protected function getHint() {
        if (isset($this->hintText)) {

            // hint is appended directly to label element
            cs()->registerScript('form-tooltip', '
$(".fr-tooltip").tooltip();                
');

            /* $htmlOptions = $this->hintOptions;
              if (isset($htmlOptions['class']))
              $htmlOptions['class'] .= ' help-block';
              else
              $htmlOptions['class'] = 'help-block';
              return '<a id="' . $id . '" class="fr-tooltip" rel="tooltip" title="' . $this->hintText . '"></a>'; */
        } else {
            return '';
        }
    }

    /**
     * Returns hint displayed below the form field
     * @return string
     */
    protected function getHintInline() {
        if (isset($this->hintInline)) {
            return CHtml::tag('p', array('class' => 'help-block span5'), $this->hintInline);
            ;
        } else {
            return '';
        }
    }

    /* protected function getAppend() {
      $append = parent::getAppend();
      return $this->getInlineHint() . $append;
      } */

    protected function processHtmlOptions() {
        parent::processHtmlOptions();
        if (isset($this->htmlOptions['hint-inline'])) {
            $this->hintInline = $this->htmlOptions['hint-inline'];
            unset($this->htmlOptions['hint-inline']);
        }
    }

    /**
     * Renders an bootstap datepicker downloaded from some example page
     * @return string the rendered content
     * @abstract
     */
    //abstract protected function datepickerField();


}
