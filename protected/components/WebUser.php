<?php

/**
 * User object accessible through Yii::app()->user or user()
 */
class WebUser extends CWebUser {

    public $_user;
    private $_roles;

    /**
     * All possible roles user can have. These names match database tables.
     * @var type 
     */
    public static $allRoles = array(
        'user',
        'admin',
       
    );
	/**
     * Is it freshrealm administrator? Should this guy have full rights?
     * @return type
     */
    public function isAdmin() {
        //return false;
        //d("hello");
        //var_dump($this);
        
        $roles = $this->getRoles();
        if (isset($roles['system_administrator'])) {
            return true;
        }
        return false;
    }
    public function getStateId() {
        //return false;
        //d("hello");
        //var_dump($this);
        
        $roles = $this->getRoles();
        if (isset($roles['state_id'])) {
            return $roles['state_id'];
        }
        return false;
    }
    // private $_active = array();

    /**
     * Fetch all possible roles from session for this user. 
     * @see User->loadRoles()
     * The roles are stored in the session
     * @return type
     */
     
     
    private function getRoles() {
        if ($this->_roles == null) {
            $this->_roles = app()->session['roles'];
        }
        return $this->_roles;
    }
	
    

    
    /**
     * Has user access to given role? For example: is('copacker') or is('food_monitor')
     * @param type $role
     * @return boolean
     */
    public function is($role){
        $roles = $this->getRoles();
        if (isset($roles[$role])) {
            return true;
        }
        return false;
    }

    /**
     * An universal way how to determine wheater the user has write access
     * to any object in the database.
     * The function works with backend users only.
     * It is important to keep id format consistent and use "connected_to" + "connected_id" for polymorfic
     * connections. 
     * @param ActiveRecord|string $object 
     * @param int $id when object is a string of the class name this is the object id
     */
    //~ public function hasAccess($object, $id = null) {
        //~ $roles = $this->getRoles();
        //~ if ($this->isAdmin()) {
            //~ return true;
        //~ }
//~ 
        //~ if (is_object($object)) {
            //~ // 1:N connections
            //~ foreach (self::$allRoles as $role) {
                //~ $roleId = $role . "_id";
                //~ if (isset($object->$roleId) && is_array($roles[$role]) && isset($roles[$role][$object->$roleId])) {
                    //~ return true;
                //~ }
            //~ }
//~ 
            //~ // Polymorphic connections
            //~ if (isset($object->connected_to) && isset($object->connected_id)) {
                //~ if (isset($roles[$object->connected_to])) {
                    //~ if (isset($roles[$object->connected_to][$object->connected_id])) {
                        //~ return true;
                    //~ }
                //~ }
            //~ }
        //~ } else if (is_string($object) && $id > 0) {
            //~ // is assigned the given role (not object)
            //~ if (isset($roles[$object])) {
                //~ if(isset($roles[$object][$id])){
                    //~ return true;
                //~ }
            //~ }
        //~ }
//~ 
        //~ return false;
    //~ }

    /**
     * returns active role in a format
     * array(
     *   'connected_to' => 'copacker|picknpack|food_monitor|connector',
     *   'connected_id' => '(int),
     *   'level' => 'access level string'
     * )
     * @return Array|null
     */
    public function getActiveRole() {
        if (isset(app()->session['active_role'])) {
            return app()->session['active_role'];
        }
        return null;
    }

    /**
     * Check permissions and set session "active_role" attribute along with
     * access_level loaded from user role table. Don't modify session attribute
     * directly.
     * 
     * @param string $role
     * @param integer $id
     * @return boolean
     */
    public function setActiveRole($role, $id) {
        if ($this->hasAccess($role, $id)) {
            $roles = $this->getRoles();
            $accessLevel = '';
            if (isset($roles[$role])) {
                $accessLevel = $roles[$role][$id]['level'];
                if (isset($roles[$role][$id]['timezone'])){
                    app()->session['timezone'] = $roles[$role][$id]['timezone'];
                } else if (isset(app()->session['timezone'])) {
                    unset(app()->session['timezone']);
                }
            }
            app()->session['active_role'] = array('connected_to' => $role, 'connected_id' => $id, 'level' => $accessLevel);
            return true;
        }
        return false;
    }

    /**
     * Grab the first available role and set it as active.
     */
    public function setHomeRole(){
        $roles = $this->getRoles();
        foreach ($roles as $role => $arr){
            if (is_array($arr)){
                foreach ($arr as $id => $level){
                    //d("set active role to ", $role, $id);
                    $this->setActiveRole($role, $id);
                    break;
                }
            }
            break;
        }
    }
    
    /**
     * Everybody is technically a customer. The customer object is created
     * the first time the user access connector website.
     * TODO
     * @return boolean
     */
    public function isCustomer() {
        return true;
        /* if (isset(app()->session['roles']['customer'])){
          return true;
          }
          return false; */
    }

    /**
     * In other words, do we have anonymous user?
     */
    public function isGuest() {
        return $this->isGuest;
    }

    /**
     * Returns user object from database
     * @return type 
     */
    function getUser() {
        if ($this->isGuest)
            return false;
        if ($this->_user === null) {
            // TODO: cache the object
            $this->_user = User::model()->findByPk($this->id);
            //$this->_user = User::findInCache($this->id);
        }
        return $this->_user;
    }

    /**
     * Cache user menu object, which is data intensive. This function is called
     * every page reload, so we want to cache it.
     */
    //~ public function getUserMenu() {
        //~ Shared::debug("loading user menu");
        //~ $menu = app()->cache->get('user-menu-' . $this->id);
        //~ if ($menu === false) {
            //~ Shared::debug("loading user menu from cache");
            //~ $menu = $this->user->getUserMenu();
            //~ app()->cache->set('user-menu-' . $this->id, $menu);
        //~ } else {
            //~ Shared::debug("found the user in cache");
        //~ }
        //~ return $menu;
    //~ }

    /**
     * Destroy all the records stored in the session. This is mainly useful
     * for testing, where we need to switch a couple of users inside one session.
     * @param type $destroySession
     */
    public function logout($destroySession = true) {
        parent::logout($destroySession);
        $this->_user = null;
    }

    public function login($identity, $duration = 0) {
        parent::login($identity, $duration);

        // load list of roles for this user
        app()->session['roles'] = $this->getUser()->loadRoles();

        $this->setHomeRole();
        
    }

    /**
     * Redirect to dashboard based on a role
     */
    public function redirectHome() {
        app()->controller->redirect($this->getHomeUrl());
    }

    /**
     * Detect a home role and redirect to that place.
     * @return type
     */
    public function getHomeUrl() {
        if ($this->isAdmin()) {
            return absUrl('/admin');
        }
        return absUrl('/');
    }

    /**
     * Redirect to login screen, when user is not logged in.
     */
    public function loginRequired() {
        if ($this->isGuest()) {
            parent::loginRequired();
        } else if ($this->isAdmin()) {
            app()->controller->redirect(url('/'));
        }

        // permission denied
        throw new CHttpException(403, 'You don\' have permission to access this page.');
    }

}
