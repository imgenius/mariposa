<?php

/**
 * Extension of data provider allowing us to attach behaviors to the models
 * used in the data provider.
 * Use $modelBehaviors variable to specifiy array of behaviors.
 * 
 * @since 2013/06/13
 * @author Ondrej Nebesky <ondrej@freshrealm.co>
 */
class ActiveDataProvider extends CActiveDataProvider {

    /**
     *
     * @var type Associative array where key is behavior name and value is the class name.
     */
    public $modelBehaviors = array();

    public function getData($refresh = false) {
        $data = parent::getData($refresh);

        if (count($this->modelBehaviors)) {
            foreach ($data as &$model) {
                foreach ($this->modelBehaviors as $name => $behavior) {
                    $model->attachBehavior($name, new $behavior);
                }
            }
        }
        return $data;
    }

}

?>
