<?php

/**
 * This is the model class for table "{{user}}".
 *
 * The followings are the available columns in table '{{user}}':
 * @property integer $id
 * @property string $username
 * @property string $password
 * @property string $salt
 * @property string $email
 * @property string $full_name
 * @property string $name
 * @property string $surname
 * @property string $address
 * @property string $address2
 * @property string $city
 * @property string $state
 * @property string $post_code
 * @property string $mobile_number
 * @property string $office_number
 * @property string $fax_number
 * @property string $company
 * @property string $comments
 * @property string $ip
 * @property string $tax_exempt
 * @property string $bill_later
 * @property string $by_check
 * @property string $by_credit
 * @property integer $logins_allowed
 * @property string $created_time
 * @property string $update_timec
 * @property integer $group_id
 * @property integer $state_id
 * @property integer $num_visits
 * @property string $last_visit
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{user}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password, email, group_id', 'required'),
			array('logins_allowed, group_id, state_id, num_visits', 'numerical', 'integerOnly'=>true),
			array('username, password, salt, email', 'length', 'max'=>128),
			array('full_name, name, surname, mobile_number, office_number, fax_number, company, comments, ip', 'length', 'max'=>255),
			array('address, address2', 'length', 'max'=>155),
			array('city', 'length', 'max'=>125),
			array('state', 'length', 'max'=>2),
			array('post_code', 'length', 'max'=>10),
			array('tax_exempt, bill_later, by_check, by_credit', 'length', 'max'=>3),
			array('create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, password, salt, email, full_name, name, surname, address, address2, city, state, post_code, mobile_number, office_number, fax_number, company, comments, ip, tax_exempt, bill_later, by_check, by_credit, logins_allowed, create_time, update_time, group_id, state_id, num_visits', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		 return array(
            //'roles' => array(self::HAS_MANY, 'UserRole', 'user_id')
        );
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'password' => 'Password',
			'salt' => 'Salt',
			'email' => 'Email',
			'full_name' => 'Full Name',
			'name' => 'Name',
			'surname' => 'Surname',
			'address' => 'Address',
			'address2' => 'Address2',
			'city' => 'City',
			'state' => 'State',
			'post_code' => 'Post Code',
			'mobile_number' => 'Mobile Number',
			'office_number' => 'Office Number',
			'fax_number' => 'Fax Number',
			'company' => 'Company',
			'comments' => 'Comments',
			'ip' => 'Ip',
			'tax_exempt' => 'Tax Exempt',
			'bill_later' => 'Bill Later',
			'by_check' => 'By Check',
			'by_credit' => 'By Credit',
			'logins_allowed' => 'Logins Allowed',
			'create_time' => 'Created At',
			'update_time' => 'Modified At',
			'group_id' => 'Group',
                    'state_id' => 'State',
			'num_visits' => 'Num Visits',
			//'last_visit' => 'Last Visit',
		);
	}


/**
     * Load list of roles from database. 
     * 
     * @return Output is an associative array, where role 
     * is and associative array of ids is a value.
     */
    public function loadRoles(){
        //$sql = "SELECT access_level, enabled, connected_to, connected_id from user_role WHERE user_id=:user_id AND enabled = 1";
        $sql = "SELECT group_id, state_id from tbl_user WHERE id=:user_id";
        $command = app()->db->createCommand($sql);
        $command->bindValue('user_id', $this->id);
        $rs = $command->query();
        $roles = array();
        foreach ($rs as $record){
            //~ if ($record['access_level'] == 'system_administrator'){
                //~ $roles['system_administrator'] = true;
                //~ // Load timezone here
            //~ } else {
                //~ $roles[$record['connected_to']][$record['connected_id']] = array('level' => $record['access_level']);
            //~ }
            if ($record['group_id'] == 1) {
				$roles['system_administrator'] = true;
			} else {
				$roles['state_id']=$record['state_id'];
			}
        }
        return $roles;
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	 
	 
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		
$criteria->compare('email',$this->email,true);
		$criteria->compare('full_name',$this->full_name,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('surname',$this->surname,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('post_code',$this->post_code,true);
		$criteria->compare('mobile_number',$this->mobile_number,true);
		$criteria->compare('office_number',$this->office_number,true);
		$criteria->compare('fax_number',$this->fax_number,true);
		$criteria->compare('company',$this->company,true);
	
		return $criteria;
	}
}
