<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class DatecalcForm extends CFormModel {

    public $day;
    public $month;
    public $year;
    public $days=0;

    /**
     * Declares the validation rules.
     * The rules state that username and password are required,
     * and password needs to be authenticated.
     */
    public function rules() {
        return array(
            // username and password are required
            array('days, day, month, year', 'required'),
            // rememberMe needs to be a boolean
           
        );
    }

    /**
     * Declares attribute labels.
     */
    public function attributeLabels() {
        return array(
            'day' => 'Days',
            //'rememberMe' => 'Remember me',
        );
    }
    
    public function calcDueDate() {
        $timestamp= strtotime($this->day . " " . $this->month . " " . $this->year) + (60 * 60 * 24 * $this->days);
        return date('F d Y', $timestamp);
    }

}
