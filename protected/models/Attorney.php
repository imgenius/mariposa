<?php

/**
 * This is the model class for table "attorneys".
 *
 * The followings are the available columns in table 'attorneys':
 * @property integer $attorneyID
 * @property string $recordID
 * @property string $attorneyName
 * @property string $county
 * @property string $firmName
 * @property string $firmabbrev
 * @property string $address1
 * @property string $address2
 * @property string $csz
 * @property string $phone
 * @property string $fax
 * @property string $DateStamp
 * @property string $Sort
 * @property string $sortName
 * @property integer $state_id
 */
class Attorney extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'attorneys';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('state_id', 'required'),
			array('state_id', 'numerical', 'integerOnly'=>true),
			array('recordID', 'length', 'max'=>11),
			array('attorneyName', 'length', 'max'=>40),
			array('county', 'length', 'max'=>15),
			array('firmName, Sort', 'length', 'max'=>75),
			array('firmabbrev', 'length', 'max'=>35),
			array('address1, address2', 'length', 'max'=>50),
			array('csz', 'length', 'max'=>45),
			array('phone', 'length', 'max'=>14),
			array('fax', 'length', 'max'=>18),
			array('DateStamp', 'length', 'max'=>19),
			array('sortName', 'length', 'max'=>100),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('attorneyID, recordID, attorneyName, county, firmName, firmabbrev, address1, address2, csz, phone, fax, DateStamp, Sort, sortName, state_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			//'creator'=>array(self::BELONGS_TO, 'User', 'create_user_id'),
			//'updator'=>array(self::BELONGS_TO, 'User', 'update_user_id'),
			'state'=>array(self::BELONGS_TO, 'State', 'state_id'),
			//'states'=>array(self::MANY_MANY, 'State', 'tbl_category_state_assignment(category_id, state_id)'),
		);	
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'attorneyID' => 'Attorney',
			'recordID' => 'Record',
			'attorneyName' => 'Attorney Name',
			'county' => 'County',
			'firmName' => 'Firm Name',
			'firmabbrev' => 'Firmabbrev',
			'address1' => 'Address1',
			'address2' => 'Address2',
			'csz' => 'Csz',
			'phone' => 'Phone',
			'fax' => 'Fax',
			'DateStamp' => 'Date Stamp',
			'Sort' => 'Sort',
			'sortName' => 'Sort Name',
			'state_id' => 'State',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('attorneyID',$this->attorneyID);
		$criteria->compare('recordID',$this->recordID,true);
		$criteria->compare('attorneyName',$this->attorneyName,true);
		$criteria->compare('county',$this->county,true);
		$criteria->compare('firmName',$this->firmName,true);
		$criteria->compare('firmabbrev',$this->firmabbrev,true);
		$criteria->compare('address1',$this->address1,true);
		$criteria->compare('address2',$this->address2,true);
		$criteria->compare('csz',$this->csz,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('DateStamp',$this->DateStamp,true);
		$criteria->compare('Sort',$this->Sort,true);
		$criteria->compare('sortName',$this->sortName,true);
		$criteria->compare('state_id',$this->state_id);

		return $criteria;
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Attorney the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
        
        public function getCounties($state_id) {
            //$command='Select Distinct(county) from attorneys where state_id='. $state_id;
           $model = Attorney::model()->findAll(array(
             'select'=>'t.county',
                 'group'=>'t.county',
                 'where' => 'state_id='. $state_id,
                 'distinct'=>true,
        ));
            return $model;
          //    $array=CHtml::listData($model, 'state_id', 'name'); 
          //  return $array;
        }
}
