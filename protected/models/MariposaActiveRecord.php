<?php


abstract class MariposaActiveRecord extends CActiveRecord
{
/** 
*Prepares create_time, create_user_id, update_time and update_user_id addtributes before validation.
*/
 public $globalSearch;

    public function rules() {
        return array(
            array('globalSearch', 'safe', 'on' => 'search'),
        );
    }

protected function beforeValidate()
{
	if ($this->isNewRecord)
	{
		//set the create date, last updated date and the user
		$this->create_time=$this->update_time=new CDbExpression('NOW()');
		$this->create_user_id=$this->update_user_id = Yii::app()->user->id;
	} else 
	{
		//not a new record just update last update time
		$this->update_time=new CDbExpression('NOW()');
		$this->update_user_id=Yii::app()->user->id;
	}
	return parent::beforeValidate();
	
}

}
