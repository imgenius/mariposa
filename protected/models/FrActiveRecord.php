<?php

class FrActiveRecord extends CActiveRecord {

    public $globalSearch;

    public function rules() {
        return array(
            array('globalSearch', 'safe', 'on' => 'search'),
        );
    }

}
