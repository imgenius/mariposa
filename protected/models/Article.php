<?php

/**
 * This is the model class for table "{{article}}".
 *
 * The followings are the available columns in table '{{article}}':
 * @property integer $id
 * @property string $name
 * @property string $content
 * @property integer $publish
 * @property integer $priority
 * @property string $css
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 * @property integer $category_id
 */
class Article extends MariposaActiveRecord
{
	//~ const TYPE_BUG=0;
	//~ const TYPE_FEATURE=1;
	//~ const TYPE_TASK=2;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Article the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{article}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('create_user_id, update_user_id, category_id', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>128),
			array('content, css, create_time, category_id, publish, priority, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, categories.name, categories, name, publish, content, category_id', 'safe', 'on'=>'search'),
			array('name, content, category_id', 'required'),
			
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'categories'=>array(self::BELONGS_TO, 'Article_Category', 'category_id'),
			'creator'=>array(self::BELONGS_TO, 'User', 'create_user_id'),
			'updator'=>array(self::BELONGS_TO, 'User', 'update_user_id'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'content' => 'Content',
			'css' => 'CSS',
                        'publish' => 'Publish',
                        'priority' => 'Priority',
			'create_time' => 'Create Time',
			'create_user_id' => 'Create User',
			'update_time' => 'Update Time',
			'update_user_id' => 'Update User',
			'category_id' => 'Category',
		);
	}
        
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
            $alias = $this->getTableAlias();

            $criteria = new CDbCriteria;
            /*
             * if ($_GET['Article%5BglobalSearch%5D']) {
				d('bad code');
			} else {
					d('ok');
			}
			* */
            d($this->globalSearch);
        if ($this->globalSearch != null) {
            if (is_numeric($this->globalSearch)) {
                $criteria->compare($alias . '.id', $this->globalSearch, true, 'OR');
            }
            
            $criteria->compare($alias . '.name', $this->globalSearch, true, 'OR');
            $criteria->compare($alias . '.content', $this->globalSearch, true, 'OR');
            $criteria->compare('categories.name', $this->globalSearch, true, 'OR');

            //$criteria->compare($alias . '.id', $this->globalSearch, true);
            $this->categories = $this->globalSearch;
            d($this->categories);
        } else {
             $criteria->with = array('categories');
            //$criteria->compare($alias . '.id', $this->id, true);
            //$criteria->compare($alias . '.name', $this->name, true);
	    //$criteria->compare($alias . '.publish', $this->publish, true);
                $criteria->compare($alias . '.name', $this->name, true);
               $criteria->compare($alias . '.publish', $this->publish, true);
               $criteria->compare('categories.name', $this->categories, true);
        }
             
            return $criteria;
		//return new CActiveDataProvider($this, array(
		//	'criteria'=>$criteria,
		//));
	}
	
	
	public function getTypeOptions() 
	{
		return array(
		  self::TYPE_BUG=>'Audi',
		  self::TYPE_FEATURE=>'Porsche',
		  self::TYPE_TASK=>'VW',
		);
	}
	public function getCategoryOptions()
	{
		
		
		$categoryArray= CHtml::listData(Article_Category::model()->findAll(), 'cat_id', 'name'); 
		return $categoryArray;
		//~ $categoryArray=array();
		//~ return $categoryArray;
	}
	public function getArticleCategoryText() 
	{
		$catOptions=$this->getCategoryOptions();
		//print_r($catOptions);
		return isset($catOptions[$this->category_id]) ? $catOptions[$this->category_id] : "unknown category ({$this->category_id})";
		//~ $typeoptions=$this->getTypeOptions;
		//~ $typename=$typeoptions[$this->status_id];
		//return "test";
	}
       

	// do our email and link regexing on the article
        /*
	public function do_article_regexing ($article2)
	{
		// make email addresses into links
		//  this MUST come BEFORE the link rewriting or else it will screw up the email addresses!
	//	$simple_email_regex = '/[a-z0-9][a-z0-9_\-\.]*@([a-z0-9][a-z0-9\-]{0,62}\.)+[a-z]+/i';
		$notalready_email_regex = '/(">|mailto:)?([a-z0-9][a-z0-9_\-\.]*@([a-z0-9][a-z0-9\-]{0,62}\.)+[a-z]+)(<\/a>|">)?/i';
	//	$article = preg_replace($notalready_email_regex,'<a href="mailto:$0">$0</a>',$article);
		$article2 = preg_replace_callback($notalready_email_regex, function ($matches)
	{
		// if this is part of a mailto: or link already, skip it 
                // this was breaking it...
        
		// build a link to this url
		return "<a href=\"mailto:$matches[2]\">$matches[2]</a>";
	}, $article2);

		// create links when there is nothing yet
		//  http://www.faqs.org/rfcs/rfc1738.html
		//  http://www.blooberry.com/indexdot/html/topics/urlencoding.htm
		$query_str_chars = '\\$\!\*\\\'\(\)\,\&\+\%\=\-\_\.0-9a-z';
		$simple_link_regex = '([a-z0-9][a-z0-9\-]{0,62}\.)+[a-z]{2,6}(\/[\~\#]?[0-9a-z\-\_]+([\.][0-9a-z]+(\?['.$query_str_chars.']*)?)?)*';
		$article2 = preg_replace_callback('/(href="?|">|src="[^"]+|@|mailto:)?(https?:\/\/)?('.$simple_link_regex.'\/?)/i', 
                      function ($matches){
                        // if this is part of an href= already, or email address, skip it
                        if ($matches[1]) return $matches[0];

                        // this is optionally matched, but if it\'s NOT there, we need to put it there
                        $matches[2] = 'http://';

                        // build a link to this url
                        return "<a href=\"$matches[2]$matches[3]\" target=\"_blank\">$matches[0]</a>";
                    },
                     $article2
                 );

		// we've regex-ed it, send it back
		return $article2;
	}

	// does the work on chunks of the article which are NOT inside javascript
	public function not_javascript_parse ($matches)
	{
		// get the text we will regex upon, from either after javascript, or before -- or all if no JS
		$article2 = ($matches[5] ? $matches[5] : ($matches[3] ? $matches[4] : $matches[1]));

		// do the actual regexing
		$this->do_article_regexing($article2);

		// is the article text after or before the script tag -- or no JS at all?
		return ($matches[5] ? $article2 : ($matches[3] ? $matches[3].$article2 : $article2.$matches[2]));
	}
         * 
         */

}
