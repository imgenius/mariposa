<?php
//echo "ART CAT MODEL";
//exit();
/**
 * This is the model class for table "{{article_category}}".
 *
 * The followings are the available columns in table '{{article_category}}':
 * @property integer $cat_id
 * @property string $name
 * @property string $slug
 * @property string $create_time
 * @property integer $create_user_id
 * @property string $update_time
 * @property integer $update_user_id
 * @property integer $parent_id
 * @property thinteger $priority
 * @property integer $publish
 * @property integer $state_id
 * @property integer $revision
 */
class Article_Category extends CActiveRecord
{
        //public $state;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Article_Category the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return '{{article_category}}';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name', 'required'),
			array('create_user_id, update_user_id, state_id,  parent_id, priority, publish, revision', 'numerical', 'integerOnly'=>true),
			array('name', 'length', 'max'=>128),
			array('slug, create_time, update_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('cat_id, state_id, state, name, parent_id, priority, publish, revision', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			
			'creator'=>array(self::BELONGS_TO, 'User', 'create_user_id'),
			'updator'=>array(self::BELONGS_TO, 'User', 'update_user_id'),
			'state'=>array(self::BELONGS_TO, 'State', 'state_id'),
			//'states'=>array(self::MANY_MANY, 'State', 'tbl_category_state_assignment(category_id, state_id)'),
			
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'cat_id' => 'Cat',
			'name' => 'Name',
			'slug' => 'slug',
			'create_time' => 'Create Time',
			'create_user_id' => 'Create User',
			'update_time' => 'Update Time',
			'update_user_id' => 'Update User',
			'parent_id' => 'Parent',
			'state_id' => 'State',
			'priority' => 'Priority',
			'publish' => 'Publish',
			'revision' => 'Revision',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
                 $alias = $this->getTableAlias();
		  $criteria = new CDbCriteria;
        $criteria->with = array('state');
        //$criteria->compare($this->getTableAlias().'.id', $this->id, true);
            //$criteria->compare($alias . '.id', $this->id, true);
            //$criteria->compare($alias . '.name', $this->name, true);
	    //$criteria->compare($alias . '.publish', $this->publish, true);
                $criteria->compare($this->getTableAlias().'.name', $this->name, true);
               // $criteria->compare($alias . '.state_id', $this->state_id, true);
               //  $criteria->compare($alias . '.slug', $this->slug, true);
                 $criteria->compare('state.name', $this->state, true);
                  // $criteria->compare($alias . '.state', $this->state, true);
            return $criteria;
	}
	/**
	 *This function draws the sidebar, sub category list, and is also used in the admin area 
	 * @param integer state_id
	 * @param boolean show subcategories
	 * @param integer specific category
	 * @return array list of categories to loop on in view
	 */
	public function getCategoryList($state='', $showsubs=1, $id=0)
	{
		if ($state>0) {
			if ($showsubs ==1) {
				$array=CHtml::listData($this->findAll(array("condition"=>"state_id =  $state  OR state_id IS NULL","order"=>"priority, name")), 'cat_id', 'name'); 
			} else {
				$array=CHtml::listData($this->findAll(array("condition"=>"(state_id =  $state OR state_id IS NULL) AND parent_id=0","order"=>"priority, name")), 'cat_id', 'name'); 
			}
		} elseif ($id > 0) {
			$array=CHtml::listData($this->findAll(array("condition"=>"parent_id =  $id","order"=>"priority, name")), 'cat_id', 'name'); 
		} else {
			$array=CHtml::listData($this->findAll(), 'cat_id', 'name'); 
		}
		return $array;
		
	}
	/**
	 *This function draws articles on the subcategory or category page
	 * @param integer article_id
	 * @return array list of articles to loop on in view
	 */
	public function getArticleList($id)
	{
		
		if ($id > 0) {
			$array=CHtml::listData(Article::model()->findAll(array("condition"=>"category_id =  $id AND publish = 1","order"=>"priority, name")), 'id', 'name'); 
		}
		//$categoryArray= CHtml::listData(Article_Category::model()->findAll(), 'cat_id', 'name'); 
		return $array;
		
	}
	
	//~ public function getStateList()
	//~ {
		//~ //should be able to use relation states?
		//~ $array=CHtml::listData(State::model()->findAll(),  'state_id', 'name'); 
		//~ return $array;
		//~ 
	//~ }
	public function getStateList()
	{
		//should be able to use relation states?
		//$array=CHtml::listData($this->states,  'state_id', 'name'); 
		$array=CHtml::listData(State::model()->findAll(),  'state_id', 'name'); 
		//var_dump($array);
		return $array;
		
	}
	//NEW function for filtering see pg.114 in Yii1.1
	public function filterStateContect($filterChain) {
		
		$filterChain->run();
	}
}
