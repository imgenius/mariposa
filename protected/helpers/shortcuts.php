<?php

// Set of function, which will simplify typing

/**
 * This is the shortcut to Yii::app()
 * @return CApplication the application singleton, null if the singleton has not been created yet.
 */
function app() {
    return Yii::app();
}

/**
 * This is the shortcut to Yii::app()->clientScript
 * @return CClientScript
 */
function cs() {
    // You could also call the client script instance via Yii::app()->clientScript
    // But this is faster
    return Yii::app()->getClientScript();
}

/**
 * This is the shortcut to CHtml::encode
 */
function h($text) {
    return htmlspecialchars($text, ENT_QUOTES, Yii::app()->charset);
}

/**
 * This is the shortcut to Yii::app()->createUrl()
 */
function url($route, $params = array(), $ampersand = '&') {
    $url = Yii::app()->createUrl($route, $params, $ampersand);
    return strlen($url) == 0 ? bu() : $url;
}

/**
 * Create absolute url using HTTPS schema
 * @param type $route
 * @param type $params
 * @param type $ampersand
 * @return type
 */
function urlSec($route, $params = array(), $ampersand = '&') {
    if (Yii::app()->params['useHttps']) {
        return Yii::app()->createAbsoluteUrl($route, $params, 'https', $ampersand);
    } else {
        return url($route, $params, $ampersand);
    }
}

function absUrl($route, $params = array(), $schema = '', $ampersand = '&') {
    return Yii::app()->createAbsoluteUrl($route, $params, $schema, $ampersand);
}

/**
 * This is the shortcut to Yii::app()->request->baseUrl
 * If the parameter is given, it will be returned and prefixed with the app baseUrl.
 */
function bu($url = null) {
    static $baseUrl = null;
    if ($baseUrl === null)
        $baseUrl = Yii::app()->getRequest()->getBaseUrl();
    return $url === null ? $baseUrl : $baseUrl . '/' . ltrim($url, '/');
}

/**
 * Another way how to debug code
 * @param type $what
 */
function fb($what) {
    echo Yii::trace(CVarDumper::dumpAsString($what), 'vardump');
}

/**
 * List of errors as inline string
 * @param type $errors
 * @return string
 */
function errs($errors) {
    $errs = '';
    foreach ($errors as $f) {
        foreach ($f as $e) {
            $errs = $errs . $e . ' ';
        }
    }
    return $errs;
}

function host() {
    return Yii::app()->getRequest()->getHostInfo();
}

function setErr($errors) {
    Yii::app()->user->setFlash('error', 'Something went wrong, try again please: ' . errs($errors));
}

/**
 * This is the shortcut to Yii::app()->user.
 * @return WebUser Web user object used for authentication and user session.
 */
function user() {
    return Yii::app()->getUser();
}

/**
 * Shortcut for debug function
 */
function d() {
    // data we are going to log
    $variables = func_get_args();

    $trace = debug_backtrace();

    $file = $trace[0]['file'];
    $line = $trace[0]['line'];
    $pos = strpos($file, 'protected');
    $file = substr($file, $pos);

    $output = "";
    $output .= $file . " (" . $line . "): ";

    foreach ($variables as $object) {
        if (strlen($output)){
            $output .= ", ";
        }
        if (is_array($object)) {
            $output .= print_r($object, 1) . "\n";
        } else if (is_object($object)) {
            if (isset($object->attributes)) {
                $output .= print_r($object->attributes, 1) . "\n";
            } else {
                $output .= print_r($object, 1) . "\n";
            }
        } else {
            $output .= $object;
        }
    }

    // write it to the file
    $path = YiiBase::getPathOfAlias('application.runtime');
    $debugFile = $path . "/debug.log";
    if (!Shared::$fh) {
        Shared::$fh = fopen($debugFile, 'a');
    }

    $output = Shared::timeDatabase(time()) . ": " . $output . "\r\n";
    fwrite(Shared::$fh, $output);
}

?>
