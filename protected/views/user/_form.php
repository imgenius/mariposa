<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form = $this->beginWidget('widgets.FrActiveForm', array(
    'id' => 'user-form',
    'type' => 'horizontal',
    'enableAjaxValidation' => false,
        )); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<?php
	echo $form->textFieldRow($model, 'full_name', array('class' => 'span6'));


echo $form->textFieldRow($model, 'company', array('class' => 'span6'));
echo $form->textFieldRow($model, 'email', array('class' => 'span6'));


 echo $form->dropDownListRow($model, 'group_id', array('2'=> 'user', '1'=>'admin'), 
   array('empty' => 'Select a group', 'class' => 'span6', 'maxlength' => 40));?>
        
    <div class="form-row row"><div class='span2'>
		<?php echo $form->labelEx($model,'state_id'); ?></div>
		<?php 
		echo $form->dropDownList($model,'state_id',   Article_Category::model()->getStateList(), array('class' => 'span6', 'empty' => '(All States)'));; ?>
		<?php echo $form->error($model,'state_id'); ?>
	</div>
<?php
echo $form->textFieldRow($model, 'username', array('class' => 'span6'));

echo $form->passwordFieldRow($model, 'password', array('class' => 'span6', 'hint' => 'The password must be at least 8 characters long'));



echo $form->textFieldRow($model, 'address', array('class' => 'span6'));

echo $form->textFieldRow($model, 'address2', array('class' => 'span6'));

echo $form->textFieldRow($model, 'city', array('class' => 'span6'));
echo $form->textFieldRow($model, 'state', array('class' => 'span6'));
echo $form->textFieldRow($model, 'post_code', array('class' => 'span6'));

echo $form->textFieldRow($model, 'mobile_number', array('class' => 'span6'));

echo $form->textFieldRow($model, 'office_number', array('class' => 'span6'));
echo $form->textFieldRow($model, 'fax_number', array('class' => 'span6'));

echo $form->textFieldRow($model, 'comments', array('class' => 'span6'));

echo $form->checkboxRow($model, 'tax_exempt');

echo $form->checkboxRow($model, 'bill_later');

echo $form->checkboxRow($model, 'by_check');

echo $form->checkboxRow($model, 'by_credit');

?>
	<!--

	<div class="row">
		<?php //echo $form->labelEx($model,'logins_allowed'); ?>
		<?php //echo $form->textField($model,'logins_allowed'); ?>
		<?php //echo $form->error($model,'logins_allowed'); ?>
	</div>
-->

	

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->
