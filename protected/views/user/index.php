<?php
/* @var $this UserController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Users',
);

$this->menu=array(
	array('label'=>'Create User', 'url'=>array('create')),
	
);
?>

<h1>Users</h1>
<?php if(Yii::app()->user->hasFlash('success')):?>
 <div class="flash-success">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
<?php endif; ?>
<?php 
/*$this->widget('bootstrap.widgets.TbListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
 */ 
  $this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'article-grid',
    'dataProvider' => $dataProvider,
     'filter' =>  $filter,
    'columns' => array(
        
        //array('name' => 'parent.category_name', 'header' => 'Parent'),
       // array('header' => 'Category', 'value' => '$data->food_monitor_id > 0 ? $data->foodMonitor->fm_name : ($data->connector_id > 0 ? $data->connector->connector_name : "")'),
        array('name' => 'full_name'),
         array('name' => 'company'),
        //array('header' => 'phone / fax', 'value'=>'$data->phone . "<BR />" . $data->fax' ), //
        // array('name' => 'mobile_number' ), //
         array('name' => 'city'),
        array('name' => 'state'),
         array('name' => 'username'),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{delete}'
        ),
    ),
)); ?>
 
