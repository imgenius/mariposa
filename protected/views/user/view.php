<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List User', 'url'=>array('index')),
	array('label'=>'Create User', 'url'=>array('create')),
	array('label'=>'Update User', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete User', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage User', 'url'=>array('admin')),
);
?>

<h1>View User #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'password',
		'salt',
		'email',
		'full_name',
		'name',
		'surname',
		'address',
		'address2',
		'city',
		'state',
		'post_code',
		'mobile_number',
		'office_number',
		'fax_number',
		'company',
		'comments',
		'ip',
		'tax_exempt',
		'bill_later',
		'by_check',
		'by_credit',
		'logins_allowed',
		'created_at',
		'modified_at',
		'group_id',
		'num_visits',
		'last_visit',
	),
)); ?>
