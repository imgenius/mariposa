<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name;
?>

<h1>Welcome to <i><?php echo CHtml::encode(Yii::app()->name); ?></i></h1>

<p>Welcome to the <i>Attorney's/Paralegal's/Secretary's</i> eBook - the online version of our popular handbook.
<br><br>
The <i> Attorney's/Paralegal's/Secretary's</i> eBook is a subscription-based internet site for  legal, government, business and information professionals. Find judges, court fees, county officials, hundreds of website links, mileage tables, zip codes and much, much more!
<br><br>
The staff at Mariposa Publishing is committed to constant improvement to provide our customers reference materials for the office.  The <i> Attorney's/Paralegal's/Secretary's</i> eBook will be completely updated on an annual basis every January.  Revisions will occur throughout the year.
<br><br>
Please send any questions and or comments to us by clicking <a href="http://mariposapublishing.com/contactusmp.php"><b>CONTACT US.</b></a></p>
<br>
