<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <div class="span9">
        <div id="content">
            <?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>
            <?php echo $content; ?>
        </div><!-- content -->
    </div>
    <div class="span3"   style="margin-top:1px;">
        <div id="sidebar">
            <?php
            if (user()->isAdmin()) {
                $this->beginWidget('zii.widgets.CPortlet', array(
                    'title' => 'Operations',
                ));
                $this->widget('bootstrap.widgets.TbMenu', array(
                    'items' => $this->menu,
                    'htmlOptions' => array('class' => 'operations'),
                ));
                $this->endWidget();
            }
            if (!user()->isGuest) {
                //echo "list categories<br />";	 
                //echo user()->name;
                $state = user()->getStateId();
                //echo "state " . $state;
                if ($state > 0) {
                    $dataProvider = new CActiveDataProvider('Article_Category', array(
                        'criteria' => array(
                            'condition' => "t.state_id=" . $state
                        ),
                        //sort should be by rfg category i think?
                        'sort' => array(
                            'defaultOrder' => 't.priority ASC'
                        ),
                        'pagination' => array('pageSize' => 100)
                    ));
                    $dataProvider = new CActiveDataProvider('Article_Category');
                    $this->renderPartial('/layouts/sidebar', array(
                        'model' => $dataProvider,
                    ));
                }
            }
            ?>
        </div><!-- sidebar -->
    </div>
</div>
<?php $this->endContent(); ?>
