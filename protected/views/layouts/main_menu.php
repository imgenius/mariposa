<?php

// it might be cached with dependency based on active role

//~ if ($activeRole == null) {
    //~ $companySettings = null;
//~ } else if ($activeRole['connected_to'] == 'copacker') {
    //~ // TODO: cache company name together with active role in session
    //~ $companySettings = array('label' => 'Company Profile', 'url' => array('copacker/settings'));
//~ } else if ($activeRole['connected_to'] == 'connector') {
    //~ $companySettings = array('label' => 'Company Profile', 'url' => array('connector/settings'));
//~ } else if ($activeRole['connected_to'] == 'food_monitor') {
    //~ $companySettings = array('label' => 'Company Profile', 'url' => array('foodMonitor/settings'));
//~ }  else if ($activeRole['connected_to'] == 'picknpack') {
    //~ $companySettings = array('label' => 'Company Profile', 'url' => array('picknpack/settings'));
//}
//d($activeRole['connected_to'] . "ddd");


$this->widget('bootstrap.widgets.TbNavbar', array(
     'collapse' => true,
              'fluid' => false,
            'fixed' => false,
    'items' => array(
        array(
            'class' => 'bootstrap.widgets.TbMenu',
             
            'items' => array(
                array('label' => 'Home', 'url' => array('/site/index')),
                
                array('label' => 'Articles', 'url' => url('/Article'), 'visible' => user()->isAdmin()),
				array('label' => 'Categories', 'url' => url('/Category'), 'visible' => user()->isAdmin()),
				array('label' => 'Users', 'url' => url('/User'), 'visible' => user()->isAdmin()),
				array('label' => 'Attorneys', 'url' => url('/Attorney'), 'visible' => user()->isAdmin()),
				array('label'=>'Search', 'url'=>array('/article/search'),'visible' => !user()->isGuest),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
                array('label' => 'Login', 'url' => array('site/login'), 'visible' => Yii::app()->user->isGuest),
                //array('label' => 'Settings', 'visible' => !user()->isGuest, 'items' => array(
                       
                //        array('label' => 'User Profile', 'url' => array('/user/settings')),
                //        array('label' => 'Logout (' . Yii::app()->user->name . ')', 'url' => array('/site/logout'))
                //    )),
                array('label' => 'Logout', 'visible' => !user()->isGuest, 'url' => array('/site/logout'))
            ),
        ),
    ),
));


?>
