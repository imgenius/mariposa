<?php
/*

$this->breadcrumbs=array(
	'Article  Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->cat_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Categories', 'url'=>array('index')),
	array('label'=>'Create Category', 'url'=>array('create')),

);
* */
?>

<h1><?php echo $model->name; ?></h1>

<?php 
//echo $model->cat_id . "list will go here";
$list= $model->getCategoryList(0,0,$model->cat_id);
$num_subcats=count($list);
if ($num_subcats > 1) {
	echo "<ul id='article_list'>";
		foreach ($list as $key => $value) {
	
			echo "<li>" . CHtml::link($value, url('/Category/list/'. $key))    . "</li>";
		}
	echo "</ul>";
       echo "<!-- " . $model->cat_id . " count arts " . $num_subcats . "-->";
	
} else {
//getArticleList
	$list= $model->getArticleList($model->cat_id);
	$num_arts=count($list);
	if ($num_arts == 1) {
		foreach ($list as $key => $value) {
			
			$this->redirect(url('/article/detail/'. $key));
			// url('/article/detail/'. $key)) . ' -- '   . (user()->isAdmin() ?  CHtml::link('<i class="icon-pencil"></i>',  url('/article/update/'. $key))  : '') . "</li>";
		}
	}
	
        //admin user add article link
        if (user()->isAdmin()) {
             echo CHtml::link('Add new Article',  url('/article/add/'. $model->cat_id)) . "<br /><br />";
        }
	echo "<ul id='article_list'>";
		foreach ($list as $key => $value) {
			
			echo "<li>" . CHtml::link($value,  url('/article/detail/'. $key)) .  (user()->isAdmin() ? ' -- '   . CHtml::link('<i class="icon-pencil"></i>',  url('/article/update/'. $key))  : '') . "</li>";
		}
	echo "</ul>";
      
        echo "<!-- " . $model->cat_id . "count arts " . $num_arts . "-->";
}

?>
