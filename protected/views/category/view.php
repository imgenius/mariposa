<?php
/* @var $this Article_CategoryController */
/* @var $model Article_Category */

$this->breadcrumbs=array(
	'Article  Categories'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Article_Category', 'url'=>array('index')),
	array('label'=>'Create Article_Category', 'url'=>array('create')),
	array('label'=>'Update Article_Category', 'url'=>array('update', 'id'=>$model->cat_id)),
	array('label'=>'Delete Article_Category', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->cat_id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Article_Category', 'url'=>array('admin')),
);
?>

<h1>View Article_Category #<?php echo $model->cat_id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'cat_id',
		'name',
		'slug',
		'create_time',
		'create_user_id',
		'update_time',
		'update_user_id',
		'state_id',
		'parent_id',
		'priority',
		'publish',
		'revision',
	),
)); ?>
