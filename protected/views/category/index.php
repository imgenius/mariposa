<?php
/* @var $this Article_CategoryController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Article  Categories',
);

$this->menu=array(
	array('label'=>'Create Article_Category', 'url'=>array('create')),
	
);

$stateArray =State::model()->getStateArray();

$buttons =array();
  $button['label']='All';
   $button['url']='?state=All';
 $buttons[]=$button;
foreach ($stateArray as $key=>$value) {
    $button= array();
    $button['label']=$value;
    $button['url']='?state='. $key;
    $buttons[]=$button;
}

        ?>
<h1>Article  Categories</h1>


<div class="btn-toolbar">
    <?php $this->widget('bootstrap.widgets.TbButtonGroup', array(
        'type'=>'primary', // '', 'primary', 'info', 'success', 'warning', 'danger' or 'inverse'
        'buttons'=>array(
            array('label'=>$defaultType, 'items'=>$buttons),
        ),
    )); ?>
</div>


<?php
/* $this->widget('bootstrap.widgets.TbListView',array(
  'dataProvider'=>$dataProvider,
  'itemView'=>'_view',
  )); */
//$a = new CButtonColumn($grid);
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'item-grid',
/*
    'filter'=>$filter,
    'dataProvider' => $dataProvider,
    'columns' => array(
    'name',
     array('header' => 'Parent', 'type' => 'raw', 'value' => '$data->parent_id ? "yes" : "no"'),
     array('header' => 'States', 'type' => 'raw', 'value' => '$data->state_id'),
     array(
        'class' => 'bootstrap.widgets.TbButtonColumn',
        'template' => '{update}{delete}',
        //'buttons' => array('add')
     ),
*/
    'filter' => $filter,
    'dataProvider' => $dataProvider,
    'columns' => array(
        'name',
         //array('header' => 'Parent', 'type' => 'raw', 'value' => '$data->parent_id ? "yes" : "no"'),
        array('name' => 'parent_id', 'value' => '$data->parent_id ? "yes" : "no"'),
        array('name' => 'state',  'value' => '$data->state->name'),
        //'available_qty',
        //'suggested_retail',
        //'_suggested_price',
        /*
          'nft_info',
          'create_time',
          'create_user_id',
          'update_time',
          'update_user_id',
          'weight',
          'height',
          'length',
          'supplier_id',
          'upc',
          'gtin',
          'prep',
          'bricks',
          'has_ingredients',
          'price_by_weight',
          'unit_weight',
         */
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{update}{delete}',
            //'buttons' => array('add')
        ),

    ),
));
?>
