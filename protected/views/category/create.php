<?php
/* @var $this Article_CategoryController */
/* @var $model Article_Category */

$this->breadcrumbs=array(
	'Article  Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Article_Category', 'url'=>array('index')),
	array('label'=>'Manage Article_Category', 'url'=>array('admin')),
);
?>

<h1>Create Article_Category</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>