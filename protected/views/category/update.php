<?php
/* @var $this Article_CategoryController */
/* @var $model Article_Category */

$this->breadcrumbs=array(
	'Article  Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->cat_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Categories', 'url'=>array('index')),
	array('label'=>'Create Category', 'url'=>array('create')),

);
?>

<h1>Update Article_Category <?php echo $model->cat_id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
