<?php
/*

$this->breadcrumbs=array(
	'Article  Categories'=>array('index'),
	$model->name=>array('view','id'=>$model->cat_id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Categories', 'url'=>array('index')),
	array('label'=>'Create Category', 'url'=>array('create')),

);
* */
?>

<h1>Date and Expiration Date Calculator</h1>
<?php
if ($date) {
    echo "<h2>Calculated Result: " . $date . "</h2>";
}
?>
<p>The following date calculator is designed to identify a date prior to, or after, a specific date for planning purposes.
 </p>
<p>To use this calculator:
 </p>
<ol>
	<li><strong>Click on the starting day, month and year</strong>

<br /><br />  This calculator does not include the first day in its computation; however, you must select the first day in order to obtain an
accurate result.
 </li>             <li><strong>Type in the number of days</strong>     
<br /><br />
To determine a deadline (or prior) date please type in a negative sign (-) and the number of days
<br />   To determine an expiration (or future) date please type in the number of days (a positive sign (+) is not required)
 </li>
<li><strong>Click on Calculate</strong>         <br />
   This calculator includes Saturdays, Sundays and legal holidays in its computation
</li>

</ol>


<?php
//$form = $this->beginWidget('widgets.FrActiveForm', array(
//        'id' => 'date-form',
//        'enableAjaxValidation' => false,
 //   ));
    
     $form=$this->beginWidget('CActiveForm', array(
	'id'=>'date-form',
	//'enableClientValidation'=>true,
	//'clientOptions'=>array(
	//	'validateOnSubmit'=>true,
	
)); 

?>
<?php echo $form->errorSummary($model); ?>

 <div class="row">
     <label>Start Date: </label>
     	<?php
        $days=array();
            for($i=1; $i<32; $i++){
                $days[$i]=$i;
            }
        echo $form->dropDownList($model,'day', $days, array("class"=>"span1")); 
        $months = array("January" => "January", "February" => "February", "March" => "March", "April"=>"April", "May"=>"May", "June"=>"June","July"=>"July", 
            "August"=>"August", "September"=>"September", "October"=>"October", "November"=>"November", "December"=>"December");
           echo $form->dropDownList($model,'month', $months, array("class"=>"span2")); 
        $todayr = date("Y");
	$years=array();							
	for($j=($todayr-1); $j<($todayr+6); $j++){
             $years[$j]=$j;
        }
         echo $form->dropDownList($model,'year', $years, array("class"=>"span1")); 
        ?>
 </div>
	<div class="row">
		<?php echo $form->labelEx($model,'days'); ?>
		<?php echo $form->textField($model,'days'); ?>
		<?php echo $form->error($model,'days'); ?>
	</div>
<?php //echo $form->textFieldRow($user, 'mobile_phone', array('class' => 'span5', 'maxlength' => 10)); ?>
<?php //echo $form->textFieldRow($user, 'first_name', array('class' => 'span5')); ?>
<?php //echo $form->textFieldRow($user, 'last_name', array('class' => 'span5')); ?>
<?php //echo $form->textFieldRow($user, 'email', array('class' => 'span5'));?>
<?php //echo $form->textFieldRow($customer, 'external_customer_id', array('class' => 'span5', 'maxlength' => 45)); ?>
<?php //echo $form->hiddenField($user, 'id'); ?>
<div style="margin-right: 270px;">
<?php
    $this->widget('bootstrap.widgets.TbButton', array(
        'size' => 'large',
        'buttonType' => 'submit',
        'type' => 'primary',
        'label' =>'Calculate',
        'htmlOptions' => array('class' => 'pull-left'),
    ));
?>
</div>
<?php $this->endWidget(); ?>
    
