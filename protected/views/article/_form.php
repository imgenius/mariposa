<?php
/* @var $this ArticleController */
/* @var $model Article */
/* @var $form CActiveForm */
?>

<div class="form">
<?php
/*
$form = $this->beginWidget('widgets.CActiveForm', array(
    'id' => 'item-form',
    'type' => 'horizontal',
    'htmlOptions' => array(
        'enctype' => 'multipart/form-data',
    ),
    'enableAjaxValidation' => false,
        ));
 * 
 */
 $form=$this->beginWidget('CActiveForm', array(
	'id'=>'article--category-form',
	'enableAjaxValidation'=>false,
)); 
?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<div class="row">
		<?php echo $form->labelEx($model,'category_id'); ?>
		<?php //echo $form->dropDownList($model,'category_id', $model->getTypeOptions());
		//$list=CHtml::listData(Article_Category::model()->findAll(), 'cat_id', 'name'); 
		//echo $form->dropDownList($model,'category_id',  $list, array('empty' => '(Select a category)'));
			echo $form->dropDownList($model,'category_id',  $model->getCategoryOptions(), array('empty' => 'Root Category'));
		
		 ?>
		<?php echo $form->error($model,'category_id'); ?>
	</div>
	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>128)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'content'); ?>
		<?php echo $form->textArea($model,'content',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'content'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'css'); ?>
		<?php echo $form->textArea($model,'css',array('rows'=>4, 'cols'=>50)); ?>
		<?php echo $form->error($model,'css'); ?>
	</div>
<div class="row">
		<?php echo $form->labelEx($model,'publish'); ?>
		<?php echo $form->radioButtonList($model,'publish',array(0=>'No', 1=>'Yes'),
                    array( 'separator' => "  " ) ); ?>
		<?php echo $form->error($model,'publish'); ?>
	</div>     
<div class="row">
		<?php echo $form->labelEx($model,'priority'); ?>
		<?php echo $form->textField($model,'priority',array('size'=>6)); ?>
		<?php echo $form->error($model,'priority'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php 
//echo Yii::app()->user->id;
$this->endWidget(); ?>

</div><!-- form -->
