<?php
/* @var $this ArticleController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Articles',
);

$this->menu=array(
	array('label'=>'Create Article', 'url'=>array('create')),
	
);
?>
<?php $this->widget('bootstrap.widgets.TbAlert', array(
        'block'=>true, // display a larger alert block?
        'fade'=>true, // use transitions?
        'closeText'=>'&times;', // close link text - if set to false, no close link is displayed
        'alerts'=>array( // configurations per alert type
            'success'=>array('block'=>true, 'fade'=>true, 'closeText'=>'&times;'), // success, info, warning, error or danger
        ),
    )); 
?>
<h1>Articles</h1>

<?php 
/*
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
 * 
 * 
 */


$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'article-grid',
    'dataProvider' => $dataProvider,
     'filter' =>  $filter,
    'columns' => array(
        'name',
        //array('name' => 'parent.category_name', 'header' => 'Parent'),
       // array('header' => 'Category', 'value' => '$data->food_monitor_id > 0 ? $data->foodMonitor->fm_name : ($data->connector_id > 0 ? $data->connector->connector_name : "")'),
        array('name' => 'categories', 'value' => '$data->categories->name'),
        array('name' => 'publish', 'value' => '$data->publish ==1 ? "yes" : "no"'),
        'priority',
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}{delete}'
        ),
    ),
));

?>
