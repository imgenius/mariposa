<?php
/* @var $model Article */
if (user()->isAdmin()) {
    echo CHtml::link('Edit This Page', url('/article/update/'.$model->id));
}
?>


<h1><?php echo $model->name; ?></h1>

<?php 
cs()->registerCss('article-specific', $model->css);

//echo $model->do_article_regexing($model->content);

//rest is commented out...
	// build an email link from the matches we get from the regex
	

function make_email ($matches)
	{
		// if this is part of a mailto: or link already, skip it 
                // //this was breaking it...
            
           // if (count($matches > 1)) {
	//	if ($matches[1]) {
            //        return $matches[0];
            //    } else if (count($matches > 3)) {
           //          return $matches[0];
           //     }
           // }
           
		// build a link to this url
		return "<a href=\"mailto:$matches[2]\">$matches[2]</a>";
	}

	// build a link from the matches we get from the regex
	function make_link ($matches)
	{
		// if this is part of an href= already, or email address, skip it
		if ($matches[1]) return $matches[0];

		// this is optionally matched, but if it's NOT there, we need to put it there
		$matches[2] = 'http://';

		// build a link to this url
		return "<a href=\"$matches[2]$matches[3]\" target=\"_blank\">$matches[0]</a>";
	}

	// do our email and link regexing on the article
	function do_article_regexing ($article2)
	{
		// make email addresses into links
		//  this MUST come BEFORE the link rewriting or else it will screw up the email addresses!
	//	$simple_email_regex = '/[a-z0-9][a-z0-9_\-\.]*@([a-z0-9][a-z0-9\-]{0,62}\.)+[a-z]+/i';
		$notalready_email_regex = '/(">|mailto:)?([a-z0-9][a-z0-9_\-\.]*@([a-z0-9][a-z0-9\-]{0,62}\.)+[a-z]+)(<\/a>|">)?/i';
	//	$article = preg_replace($notalready_email_regex,'<a href="mailto:$0">$0</a>',$article);
		$article2 = preg_replace_callback($notalready_email_regex, 'make_email', $article2);

		// create links when there is nothing yet
		//  http://www.faqs.org/rfcs/rfc1738.html
		//  http://www.blooberry.com/indexdot/html/topics/urlencoding.htm
		$query_str_chars = '\\$\!\*\\\'\(\)\,\&\+\%\=\-\_\.0-9a-z';
		$simple_link_regex = '([a-z0-9][a-z0-9\-]{0,62}\.)+[a-z]{2,6}(\/[\~\#]?[0-9a-z\&\-\_]+([\.][0-9a-z]+(\?['.$query_str_chars.']*)?)?)*';
		$article2 = preg_replace_callback('/(href="?|">|src="[^"]+|@|mailto:)?(https?:\/\/)?('.$simple_link_regex.'\/?)/i', 'make_link', $article2);

		// we've regex-ed it, send it back
		return $article2;
	}

	// does the work on chunks of the article which are NOT inside javascript
	function not_javascript_parse ($matches)
	{
		// get the text we will regex upon, from either after javascript, or before -- or all if no JS
		$article2 = ($matches[5] ? $matches[5] : ($matches[3] ? $matches[4] : $matches[1]));

		// do the actual regexing
		do_article_regexing($article2);

		// is the article text after or before the script tag -- or no JS at all?
		return ($matches[5] ? $article2 : ($matches[3] ? $matches[3].$article2 : $article2.$matches[2]));
	}

	// if this article has javascript, we have to skip the JS, otherwise, just parse the whole thing

        
	if (preg_match('/<script/',$model->content))
	 { $article = preg_replace_callback('/^(.*)(<script)|(\/script>)(.*)$/is', 'not_javascript_parse', $model->content); }
	else { $article = do_article_regexing($model->content); }
        

	// all done with changes
	echo do_article_regexing(str_replace('&#9;', '&nbsp;&nbsp;&nbsp;&nbsp;', $model->content));
              
//echo ; 

 ?>
