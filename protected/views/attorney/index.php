<?php
/* @var $this AttorneyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Attorneys',
);

?>

<h1>Attorneys</h1>

<?php //$this->widget('zii.widgets.CListView', array(
	//'dataProvider'=>$dataProvider,
	//'itemView'=>'_view',
//));
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'article-grid',
    'dataProvider' => $dataProvider,
     'filter' =>  $filter,
    'columns' => array(
        'name',
        //array('name' => 'parent.category_name', 'header' => 'Parent'),
       // array('header' => 'Category', 'value' => '$data->food_monitor_id > 0 ? $data->foodMonitor->fm_name : ($data->connector_id > 0 ? $data->connector->connector_name : "")'),
        array('name' => 'attorneyName'),
        array('name' => 'phone'),
        array('name' => 'county'),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => '{view}{update}{delete}'
        ),
    ),
));
 ?>
