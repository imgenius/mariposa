<?php
/* @var $this AttorneyController */
/* @var $model Attorney */

$this->breadcrumbs=array(
	'Attorneys'=>array('index'),
	$model->attorneyID=>array('view','id'=>$model->attorneyID),
	'Update',
);

$this->menu=array(
	array('label'=>'List Attorney', 'url'=>array('index')),
	array('label'=>'Create Attorney', 'url'=>array('create')),
	array('label'=>'View Attorney', 'url'=>array('view', 'id'=>$model->attorneyID)),
	array('label'=>'Manage Attorney', 'url'=>array('admin')),
);
?>

<h1>Update Attorney <?php echo $model->attorneyID; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>