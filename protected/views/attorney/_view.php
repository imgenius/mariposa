<?php
/* @var $this AttorneyController */
/* @var $data Attorney */
?>

<div class="view attorney">
<?php echo CHtml::encode($data->attorneyName) . " &#0151" . CHtml::encode($data->county); ?>
	<span class="phone"><?php echo CHtml::encode($data->phone); ?></span>
</div>
	