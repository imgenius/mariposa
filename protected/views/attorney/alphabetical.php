<?php
/* @var $this AttorneyController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Attorneys',
);

?>

<h1>Attorneys Alphabetical - <?php echo $letter; ?></h1>
<a href="?state_id=<?php echo $state_id;?>&letter=A">A</a> | <a href="?state_id=<?php echo $state_id;?>&letter=B">B</a> | <a href="?state_id=<?php echo $state_id;?>&letter=C">C</a> | 
<a href="?state_id=<?php echo $state_id;?>&letter=D">D</a> | <a href="?state_id=<?php echo $state_id;?>&letter=E">E</a> | <a href="?state_id=<?php echo $state_id;?>&letter=F">F</a> | 
<a href="?state_id=<?php echo $state_id;?>&letter=G">G</a> | <a href="?state_id=<?php echo $state_id;?>&letter=H">H</a> | <a href="?state_id=<?php echo $state_id;?>&letter=I">I</a> | 
<a href="?state_id=<?php echo $state_id;?>&letter=J">J</a> | <a href="?state_id=<?php echo $state_id;?>&letter=K">K</a> | <a href="?state_id=<?php echo $state_id;?>&letter=L">L</a> | 
<a href="?state_id=<?php echo $state_id;?>&letter=M">M</a> | <a href="?state_id=<?php echo $state_id;?>&letter=N">N</a> | <a href="?state_id=<?php echo $state_id;?>&letter=O">O</a> | 
<a href="?state_id=<?php echo $state_id;?>&letter=P">P</a> | <a href="?state_id=<?php echo $state_id;?>&letter=Q">Q</a> | <a href="?state_id=<?php echo $state_id;?>&letter=R">R</a> | 
<a href="?state_id=<?php echo $state_id;?>&letter=S">S</a> | <a href="?state_id=<?php echo $state_id;?>&letter=T">T</a> | <a href="?state_id=<?php echo $state_id;?>&letter=U">U</a> | 
<a href="?state_id=<?php echo $state_id;?>&letter=V">V</a> | <a href="?state_id=<?php echo $state_id;?>&letter=W">W</a> | <a href="?state_id=<?php echo $state_id;?>&letter=X">X</a> | 
<a href="?state_id=<?php echo $state_id;?>&letter=Y">Y</a> | <a href="?state_id=<?php echo $state_id;?>&letter=Z">Z</a>
<?php
/*
$this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); 
*/
if (user()->isAdmin()) {
    $template='{view}{update}{delete}';
} else {
    $template='{view}';
}
$this->widget('bootstrap.widgets.TbGridView', array(
    'id' => 'article-grid',
    'dataProvider' => $dataProvider,
     'filter' =>  $filter,
    'columns' => array(
        
        //array('name' => 'parent.category_name', 'header' => 'Parent'),
       // array('header' => 'Category', 'value' => '$data->food_monitor_id > 0 ? $data->foodMonitor->fm_name : ($data->connector_id > 0 ? $data->connector->connector_name : "")'),
        array('name' => 'attorneyName'),
         array('name' => 'firmName'),
        //array('header' => 'phone / fax', 'value'=>'$data->phone . "<BR />" . $data->fax' ), //
         array('name' => 'phone' ), //
       
        array('name' => 'county'),
        array(
            'class' => 'bootstrap.widgets.TbButtonColumn',
            'template' => $template
        ),
    ),
));
?>
 
