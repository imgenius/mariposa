<?php
/* @var $this AttorneyController */
/* @var $model Attorney */

$this->breadcrumbs=array(
	'Attorneys'=>array('alph'),
	$model->attorneyID,
);

$this->menu=array(
	array('label'=>'List Attorney', 'url'=>array('index')),
	array('label'=>'Create Attorney', 'url'=>array('create')),
	array('label'=>'Update Attorney', 'url'=>array('update', 'id'=>$model->attorneyID)),
	array('label'=>'Delete Attorney', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->attorneyID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Attorney', 'url'=>array('admin')),
);
?>

<h1><?php echo $model->attorneyName; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		
		'attorneyName',
                'firmName',
                  'phone',
		'fax',
                 'address1',
		'address2',
		'csz',
		'county',
		
		
		
		
		
		
	),
)); ?>
