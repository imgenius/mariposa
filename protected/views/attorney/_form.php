<?php
/* @var $this AttorneyController */
/* @var $model Attorney */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'attorney-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'recordID'); ?>
		<?php echo $form->textField($model,'recordID',array('size'=>11,'maxlength'=>11)); ?>
		<?php echo $form->error($model,'recordID'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'attorneyName'); ?>
		<?php echo $form->textField($model,'attorneyName',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'attorneyName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'county'); ?>
		<?php echo $form->textField($model,'county',array('size'=>15,'maxlength'=>15)); ?>
		<?php echo $form->error($model,'county'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'firmName'); ?>
		<?php echo $form->textField($model,'firmName',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'firmName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'firmabbrev'); ?>
		<?php echo $form->textField($model,'firmabbrev',array('size'=>35,'maxlength'=>35)); ?>
		<?php echo $form->error($model,'firmabbrev'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address1'); ?>
		<?php echo $form->textField($model,'address1',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'address1'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'address2'); ?>
		<?php echo $form->textField($model,'address2',array('size'=>50,'maxlength'=>50)); ?>
		<?php echo $form->error($model,'address2'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'csz'); ?>
		<?php echo $form->textField($model,'csz',array('size'=>45,'maxlength'=>45)); ?>
		<?php echo $form->error($model,'csz'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'phone'); ?>
		<?php echo $form->textField($model,'phone',array('size'=>14,'maxlength'=>14)); ?>
		<?php echo $form->error($model,'phone'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'fax'); ?>
		<?php echo $form->textField($model,'fax',array('size'=>18,'maxlength'=>18)); ?>
		<?php echo $form->error($model,'fax'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'DateStamp'); ?>
		<?php echo $form->textField($model,'DateStamp',array('size'=>19,'maxlength'=>19)); ?>
		<?php echo $form->error($model,'DateStamp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'Sort'); ?>
		<?php echo $form->textField($model,'Sort',array('size'=>60,'maxlength'=>75)); ?>
		<?php echo $form->error($model,'Sort'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sortName'); ?>
		<?php echo $form->textField($model,'sortName',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'sortName'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'state_id'); ?>
		<?php echo $form->textField($model,'state_id'); ?>
		<?php echo $form->error($model,'state_id'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->