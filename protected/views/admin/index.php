<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'article--category-form',
	'enableAjaxValidation'=>false,
)); 


$goTo=url('/admin/list');

cs()->registerScript('state select', '
$(".statesel").change(function(){

    var val1 =$(this).val();
    //alert(val1);
    var myurl = "'. $goTo .'" + "?state=" + val1;
    window.location.href = myurl;
    //$.ajax({
    //        type: "GET",
    //        url: myurl,
    //        data: page,
    //        success: function(content) {
    //           $("#categories").html(content);  
    //        }
    //    });
    });
');
?>


 <div id='info'>
       From this page, one can select a state and then follow the links to an article that you would like to edit by clicking the pencil icon to the right of the article name.  
    </div>
	<div class="row">
            <h4> Choose State </h4>
		
		<?php 
             
		echo $form->dropDownList($model,'state_id',   $model->getStateList(), array('empty' => 'Select State', 'class'=>'statesel'));; ?>
		<?php echo $form->error($model,'state_id'); ?>
	</div>
   
	

<?php $this->endWidget(); ?>

</div><!-- form -->
<div id="categories">
    <?php if ($state > 0) {
    $list=Article_Category::model()->getCategoryList($state, false);
//~ print_r($list);
echo "<ul id='cat_side'>";
foreach ($list as $key => $value) {
	
	echo "<li>" . CHtml::link($value, url('/Category/list/'. $key))    . "</li>";
}
echo "</ul>";
    }
    ?>
    
</div>